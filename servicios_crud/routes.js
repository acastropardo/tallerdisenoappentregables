const router = require('express').Router();
const validators = require('./validators');
const userController = require('./controllers/userController');
const idiomaController = require('./controllers/idiomaController');
const autorController = require('./controllers/autorController');
const categoriaController = require('./controllers/categoriaController');
const estado_libroController = require('./controllers/estado_libroController');
const editorialController = require('./controllers/editorialController');
const metodo_pagoController = require('./controllers/metodo_pagoController');
const distribuidorController = require('./controllers/distribuidorController');
const clienteController = require('./controllers/clienteController');
const loginController = require('./controllers/loginController');
const libroController = require('./controllers/libroController');
const arriendoController = require('./controllers/arriendoController');
const detalle_arriendoController = require('./controllers/detalle_arriendoController');

// funcionalidad Login
router.post(
    '/login',
    loginController.login
);

// Insertando arriendo
router.post(
    '/insert-arriendo',
    //validators.userInfo,
    validators.result,
    arriendoController.insert
    );

// Insertando detalle arriendo
router.post(
    '/insert-detalle_arriendo',
    //validators.userInfo,
    validators.result,
    detalle_arriendoController.insert
    );

// Insertando Distribuidor
router.post(
    '/insert-distribuidor',
    //validators.userInfo,
    validators.result,
    distribuidorController.insert
    );

// Inserting User
router.post(
    '/insert-user',
    validators.userInfo,
    validators.result,
    userController.insert
    );

// Insertando Autor
router.post(
    '/insert-autor',
    //validators.userInfo,
    validators.result,
    autorController.insert
    );

// Insertando Libro
router.post(
    '/insert-libro',
    //validators.userInfo,
    validators.result,
    libroController.insert
    );

// Insertando cliente
router.post(
    '/insert-cliente',
    //validators.userInfo,
    validators.result,
    clienteController.insert
    );

// Insertando Idioma
router.post(
    '/insert-idioma',
    //validators.userInfo,
    //validators.result,
    idiomaController.insert
    );

// Insertando editorial
router.post(
    '/insert-editorial',
    //validators.userInfo,
    //validators.result,
    editorialController.insert
    );

// Insertando estado_libro
router.post(
    '/insert-estado_libro',
    //validators.userInfo,
    //validators.result,
    estado_libroController.insert
    );

// Insertando estado_libro
router.post(
    '/insert-metodo_pago',
    //validators.userInfo,
    //validators.result,
    metodo_pagoController.insert
    );

// Insertando categoria
router.post(
        '/insert-categoria',
        //validators.userInfo,
        //validators.result,
    categoriaController.insert
    );

// Leyendo todos los arriendos
router.get(
    '/get-all-arriendo',
    arriendoController.getAll
    );

// Leyendo todos los detalles_arriendos
router.get(
    '/get-all-detalle_arriendo',
    detalle_arriendoController.getAll
    );

// Leyendo todos los distribuidores
router.get(
    '/get-all-distribuidor',
    distribuidorController.getAll
    );

// Fetching all users
router.get(
    '/get-all-users',
    userController.getAllUsers
    );

// Leyendo todos los autores
router.get(
    '/get-all-autor',
    autorController.getAll
    );

// Leyendo todos los libros
router.get(
    '/get-all-libro',
    libroController.getAll
    );

// Leyendo todos los clientes
router.get(
    '/get-all-cliente',
    clienteController.getAll
    );

// Leyendo todos los idiomas
router.get(
    '/get-all-idioma',
    idiomaController.getAll
    );

// Leyendo todas las editoriales
router.get(
    '/get-all-editorial',
    editorialController.getAll
    );

// Leyendo todos los estados libro
router.get(
    '/get-all-estado_libro',
    estado_libroController.getAll
    );

// Leyendo todos los metodos_pago
router.get(
    '/get-all-metodo_pago',
    metodo_pagoController.getAll
    );

// Leyendo todas las categorias
router.get(
    '/get-all-categoria',
    categoriaController.getAll
    );

// Leyendo distribuidor por id
router.get(
    '/get-distribuidor/:id',
    validators.ID,
    validators.result,
    distribuidorController.getOne
    );

// Fetching Single User By ID
router.get(
    '/get-user/:id',
    validators.userID,
    validators.result,
    userController.getUserByID
    );

// Leyendo arriendo por id
router.get(
    '/get-arriendo/:id',
    validators.ID,
    validators.result,
    arriendoController.getOne
    );

// Leyendo detalle_arriendo por id
router.get(
    '/get-detalle_arriendo/:id',
    validators.ID,
    validators.result,
    detalle_arriendoController.getOne
    );

// Leyendo detalle_arriendo por idarriendo
router.get(
    '/get-detalle_por_arriendo/:id',
    validators.ID,
    validators.result,
    detalle_arriendoController.getDetaArriendo
    );

// Leyendo autor por id
router.get(
    '/get-autor/:id',
    validators.ID,
    validators.result,
    autorController.getOne
    );

// Leyendo libro por id
router.get(
    '/get-libro/:id',
    validators.ID,
    validators.result,
    libroController.getOne
    );

// Leyendo cliente por id
router.get(
    '/get-cliente/:id',
    validators.ID,
    validators.result,
    clienteController.getOne
    );

// Leyendo idioma por id
router.get(
    '/get-idioma/:id',
    validators.ID,
    validators.result,
    idiomaController.getOne
    );

// Leyendo editorial por id
router.get(
    '/get-editorial/:id',
    validators.ID,
    validators.result,
    editorialController.getOne
    );

// Leyendo estado_libro por id
router.get(
    '/get-estado_libro/:id',
    validators.ID,
    validators.result,
    estado_libroController.getOne
    );

// Leyendo metodo_pago por id
router.get(
    '/get-metodo_pago/:id',
    validators.ID,
    validators.result,
    metodo_pagoController.getOne
    );

// Leyendo categoria por id
router.get(
    '/get-categoria/:id',
    validators.ID,
    validators.result,
    categoriaController.getOne
    );

// Actualizando arriendo
router.patch(
    '/update-arriendo/:id',
    [...validators.ID],
    validators.result,
    arriendoController.update
    );

// Actualizando detalle_arriendo
router.patch(
    '/update-detalle_arriendo/:id',
    [...validators.ID],
    validators.result,
    detalle_arriendoController.update
    );

// Actualizando distribuidor
router.patch(
    '/update-distribuidor/:id',
    [...validators.ID],
    validators.result,
    distribuidorController.update
    );

// Updating User
router.patch(
    '/update-user/:id',
    [...validators.userID, ...validators.userInfo],
    validators.result,
    userController.updateUser
    );

// Actualizando idioma
router.patch(
    '/update-idioma/:id',
    [...validators.ID],
    validators.result,
    idiomaController.update
    );

// Actualizando estado_libro
router.patch(
    '/update-estado_libro/:id',
    [...validators.ID],
    validators.result,
    estado_libroController.update
    );

// Actualizando metodo_pago
router.patch(
    '/update-metodo_pago/:id',
    [...validators.ID],
    validators.result,
    metodo_pagoController.update
    );

// Actualizando categoria
router.patch(
    '/update-categoria/:id',
    [...validators.ID],
    validators.result,
    categoriaController.update
    );

// Actualizando autor
router.patch(
    '/update-autor/:id',
    [...validators.ID],
    validators.result,
    autorController.update
    );

// Actualizando libro
router.patch(
    '/update-libro/:id',
    [...validators.ID],
    validators.result,
    libroController.update
    );

// Actualizando cliente
router.patch(
    '/update-cliente/:id',
    [...validators.ID],
    validators.result,
    clienteController.update
    );

// Eliminando distribuidor
router.delete(
    '/delete-distribuidor/:id',
    validators.ID,
    validators.result,
    distribuidorController.delete
    );

// Eliminando arriendo
router.delete(
    '/delete-arriendo/:id',
    validators.ID,
    validators.result,
    arriendoController.delete
    );

// Eliminando detalle_arriendo
router.delete(
    '/delete-detalle_arriendo/:id',
    validators.ID,
    validators.result,
    detalle_arriendoController.delete
    );

// Deleting User
router.delete(
    '/delete-user/:id',
    validators.userID,
    validators.result,
    userController.deleteUser
    );

// Eliminando autor
router.delete(
    '/delete-autor/:id',
    validators.ID,
    validators.result,
    autorController.delete
    );

// Eliminando libro
router.delete(
    '/delete-libro/:id',
    validators.ID,
    validators.result,
    libroController.delete
    );

// Eliminando cliente
router.delete(
    '/delete-cliente/:id',
    validators.ID,
    validators.result,
    clienteController.delete
    );

// Eliminando idioma
router.delete(
    '/delete-idioma/:id',
    validators.ID,
    validators.result,
    idiomaController.delete
    );

// Eliminando estado_libro
router.delete(
    '/delete-estado_libro/:id',
    validators.ID,
    validators.result,
    estado_libroController.delete
    );

// Eliminando estado_libro
router.delete(
    '/delete-metodo_pago/:id',
    validators.ID,
    validators.result,
    metodo_pagoController.delete
    );

// Eliminando categoria
router.delete(
    '/delete-categoria/:id',
    validators.ID,
    validators.result,
    categoriaController.delete
    );

module.exports = router;