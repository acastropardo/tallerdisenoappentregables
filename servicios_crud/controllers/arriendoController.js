const db_connection = require("../db-connection").promise();

// Insertando arriendo
exports.insert = async (req, res, next) => {

  if (!req.body.costo_total || !req.body.fecha_arriendo || !req.body.fecha_devolucion_estimada || !req.body.fecha_entrega_real || !req.body.dias_retraso || !req.body.multa || !req.body.costo_arriendo || !req.body.cliente_idcliente || !req.body.trabajador_idtrabajador ) {
    return res.status(400).json({
      message: "Por favor ingrese todos los campos obligatorios.",
      fields: ["costo_total","fecha_arriendo", "fecha_devolucion_estimada","fecha_entrega_real","dias_retraso","multa","costo_arriendo","cliente_idcliente","trabajador_idtrabajador"]
    });
  }

  try {
      
    const [rows] = await db_connection.execute(
      "INSERT INTO `arriendo`(`costo_total`,`fecha_arriendo`,`fecha_devolucion_estimada`,`fecha_entrega_real`,`dias_retraso`,`multa`,`costo_arriendo`,`cliente_idcliente`,`trabajador_idtrabajador` ) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)",
      [req.body.costo_total, req.body.fecha_arriendo, req.body.fecha_devolucion_estimada, req.body.fecha_entrega_real, req.body.dias_retraso, req.body.multa, req.body.costo_arriendo, req.body.cliente_idcliente, req.body.trabajador_idtrabajador]
    );

    if (rows.affectedRows === 1) {
      return res.status(201).json({
        message: "El arriendo ha sido insertado correctamente.",
        idarriendo: rows.insertId,
      });
    }

  } catch (err) {
    next(err);
  }
  
};

// Leyendo todos los arriendos
exports.getAll = async (req, res, next) => {
  try {

    const [rows] = await db_connection.execute("select * from arriendo as ar "+ 
    "inner join detalle_arriendo as b on ar.idarriendo  = b.arriendo_idarriendo "+
    "inner join libro as l on b.libro_idlibro = l.idlibro "+
    "inner join cliente as c on ar.cliente_idcliente = c.idcliente "+
    "inner join editorial as e on e.ideditorial = l.editorial_ideditorial "+
    "inner join idioma as i on l.idioma_ididioma = i.ididioma "+
    "inner join categoria as cate on l.categoria_idcategoria = cate.idcategoria ");

    if (rows.length === 0) {
      return res.status(200).json({
        message:
          "Aún no hay registros en la tabla arriendo.",
      });
    }

    res.status(200).json(rows);

  } catch (err) {
    next(err);
  }

};


// Leyendo arriendo por ID
exports.getOne = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `arriendo` WHERE `idarriendo`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "No se encontró arriendo!",
      });
    }

    res.status(200).json(row[0]);

  } catch (err) {
    next(err);
  }

};

// Actualizando arriendo
exports.update = async (req, res, next) => {
  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `arriendo` WHERE `idarriendo`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "idarriendo no valido",
      });
    }

    if (req.body.costo_total) row[0].costo_total = req.body.costo_total;
    if (req.body.fecha_arriendo) row[0].fecha_arriendo = req.body.fecha_arriendo;
    if (req.body.fecha_devolucion_estimada) row[0].fecha_devolucion_estimada = req.body.fecha_devolucion_estimada;
    if (req.body.fecha_entrega_real) row[0].fecha_entrega_real = req.body.fecha_entrega_real;
    if (req.body.dias_retraso) row[0].dias_retraso = req.body.dias_retraso;
    if (req.body.multa) row[0].multa = req.body.multa;
    if (req.body.costo_arriendo) row[0].costo_arriendo = req.body.costo_arriendo;
    if (req.body.cliente_idcliente) row[0].cliente_idcliente = req.body.cliente_idcliente;
    if (req.body.trabajador_idtrabajador) row[0].trabajador_idtrabajador = req.body.trabajador_idtrabajador;

  
    const [update] = await db_connection.execute(
      "UPDATE `arriendo` SET `costo_total`=?, `fecha_arriendo`=?, `fecha_devolucion_estimada`=?,`fecha_entrega_real`=?,`dias_retraso`=?,`multa`=?,`costo_arriendo`=?,`cliente_idcliente`=?,`trabajador_idtrabajador`=?  WHERE `idarriendo`=?",
      [row[0].costo_total, row[0].fecha_arriendo, row[0].fecha_devolucion_estimada, req.fecha_entrega_real[0], req.dias_retraso[0], req.multa[0], req.costo_arriendo[0], req.cliente_idcliente[0], req.trabajador_idtrabajador[0], req.params.id]
    );

    if (update.affectedRows === 1) {
      return res.json({
        message: "El arriendo ha sido actualizado correctamente.",
      });
    }

  } catch (err) {
    next(err);
  }

};

// Eliminando arriendo
exports.delete = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "DELETE FROM `arriendo` WHERE `idarriendo`=?",
        [req.params.id]
    );

    if (row.affectedRows === 0) {
      return res.status(404).json({
        message: "id de arriendo invalido ID (No se encontró!)",
      });
    }

    res.status(200).json({
      message: "El arriendo ha sido eliminado correctamente.",
    });
    
  } catch (err) {
    next(err);
  }

};
