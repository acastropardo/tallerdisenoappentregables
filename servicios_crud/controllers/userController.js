const db_connection = require("../db-connection").promise();

// INSERTING USER
exports.insert = async (req, res, next) => {

  if (!req.body.name || !req.body.email || !req.body.password) {
    return res.status(400).json({
      message: "Favor llene todos los campos.",
      fields: ["name", "email", "password"],
    });
  }

  try {
      
    const [rows] = await db_connection.execute(
      "INSERT INTO `users`(`name`,`email`,`password`) VALUES(?, ?, ?)",
      [req.body.name, req.body.email, req.body.password]
    );

    if (rows.affectedRows === 1) {
      return res.status(201).json({
        message: "El usuario ha sido ingresado correctamente.",
        userID: rows.insertId,
      });
    }

  } catch (err) {
    next(err);
  }
  
};

// FETCHING ALL USERS
exports.getAllUsers = async (req, res, next) => {
  try {

    const [rows] = await db_connection.execute("SELECT * FROM `users`");

    if (rows.length === 0) {
      return res.status(200).json({
        message:
          "Aun no hay usuarios en la base de datos.",
      });
    }

    res.status(200).json(rows);

  } catch (err) {
    next(err);
  }

};


// FETCHING SINGLE USER
exports.getUserByID = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `users` WHERE `id`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "No se encontró usuario!",
      });
    }

    res.status(200).json(row[0]);

  } catch (err) {
    next(err);
  }

};

// UPDATING USER
exports.updateUser = async (req, res, next) => {
  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `users` WHERE `id`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "Invalid User ID",
      });
    }

    if (req.body.name) row[0].name = req.body.name;

    if (req.body.email) row[0].email = req.body.email;

    if (req.body.password) row[0].password = req.body.password;

    const [update] = await db_connection.execute(
      "UPDATE `users` SET `name`=?, `email`=?, `password`=? WHERE `id`=?",
      [row[0].name, row[0].email, row[0].password, req.params.id]
    );

    if (update.affectedRows === 1) {
      return res.json({
        message: "El usuario ha sido actualizado correctamente.",
      });
    }

  } catch (err) {
    next(err);
  }

};

// DELETING USER
exports.deleteUser = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "DELETE FROM `users` WHERE `id`=?",
        [req.params.id]
    );

    if (row.affectedRows === 0) {
      return res.status(404).json({
        message: "Invalid user ID (No User Found!)",
      });
    }

    res.status(200).json({
      message: "El usuario ha sido eliminado correctamente.",
    });
    
  } catch (err) {
    next(err);
  }

};
