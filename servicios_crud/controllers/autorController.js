const db_connection = require("../db-connection").promise();

// Insertando autor
exports.insert = async (req, res, next) => {

  if (!req.body.nombres || !req.body.apellido_paterno || !req.body.apellido_materno) {
    return res.status(400).json({
      message: "Por favor ingrese todos los campos obligatorios.",
      fields: ["nombres","apellido_paterno", "apellido_materno"],
    });
  }

  try {
      
    const [rows] = await db_connection.execute(
      "INSERT INTO `autor`(`nombres`,`apellido_paterno`,`apellido_materno` ) VALUES(?, ?, ?)",
      [req.body.nombres, req.body.apellido_paterno, req.body.apellido_materno]
    );

    if (rows.affectedRows === 1) {
      return res.status(201).json({
        message: "El autor ha sido insertado correctamente.",
        IDAUTOR: rows.insertId,
      });
    }

  } catch (err) {
    next(err);
  }
  
};

// Leyendo todos los autores
exports.getAll = async (req, res, next) => {
  try {

    const [rows] = await db_connection.execute("SELECT * FROM `autor`");

    if (rows.length === 0) {
      return res.status(200).json({
        message:
          "Aún no hay registros en la tabla autor.",
      });
    }

    res.status(200).json(rows);

  } catch (err) {
    next(err);
  }

};


// Leyendo autor por ID
exports.getOne = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `autor` WHERE `idautor`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "No se encontró autor!",
      });
    }

    res.status(200).json(row[0]);

  } catch (err) {
    next(err);
  }

};

// Actualizando autor
exports.update = async (req, res, next) => {
  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `autor` WHERE `idautor`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "idautor no valido",
      });
    }

    if (req.body.nombres) row[0].nombres = req.body.nombres;

    if (req.body.apellido_paterno) row[0].apellido_paterno = req.body.apellido_paterno;

    if (req.body.apellido_materno) row[0].apellido_materno = req.body.apellido_materno;

    const [update] = await db_connection.execute(
      "UPDATE `autor` SET `nombres`=?, `apellido_paterno`=?, `apellido_materno`=? WHERE `id`=?",
      [row[0].nombres, row[0].apellido_paterno, row[0].apellido_materno, req.params.id]
    );

    if (update.affectedRows === 1) {
      return res.json({
        message: "El autor ha sido actualizado correctamente.",
      });
    }

  } catch (err) {
    next(err);
  }

};

// Eliminando autor
exports.delete = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "DELETE FROM `autor` WHERE `idautor`=?",
        [req.params.id]
    );

    if (row.affectedRows === 0) {
      return res.status(404).json({
        message: "id de Autor invalido ID (No se encontró!)",
      });
    }

    res.status(200).json({
      message: "El autor ha sido eliminado correctamente.",
    });
    
  } catch (err) {
    next(err);
  }

};
