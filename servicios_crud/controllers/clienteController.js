const db_connection = require("../db-connection").promise();

// Insertando cliente
exports.insert = async (req, res, next) => {

  if (!req.body.nombres || !req.body.apellido_paterno || !req.body.apellido_materno || !req.body.rut || !req.body.fecha_nacimiento) {
    return res.status(400).json({
      message: "Por favor ingrese todos los campos obligatorios.",
      fields: ["nombres","apellido_paterno", "apellido_materno","rut","fecha_nacimiento"],
    });
  }

  try {
      
    const [rows] = await db_connection.execute(
      "INSERT INTO `cliente`(`rut`,`nombres`,`apellido_paterno`,`apellido_materno`,`fecha_nacimiento`, `telefono_idtelefono`,`direccion_iddireccion`,`correo_electronico_idcorreo_electronico`) VALUES(?, ?, ?, ?, ?, ?,?,?)",
      [req.body.rut, req.body.nombres, req.body.apellido_paterno, req.body.apellido_materno, req.body.fecha_nacimiento,"1","1","1"]
    );

    if (rows.affectedRows === 1) {
      return res.status(201).json({
        message: "El cliente ha sido insertado correctamente.",
        idcliente: rows.insertId,
      });
    }

  } catch (err) {
    next(err);
  }
  
};

// Leyendo todos los autores
exports.getAll = async (req, res, next) => {
  try {

    const [rows] = await db_connection.execute("SELECT * FROM `cliente`");

    if (rows.length === 0) {
      return res.status(200).json({
        message:
          "Aún no hay registros en la tabla autor.",
      });
    }

    res.status(200).json(rows);

  } catch (err) {
    next(err);
  }

};


// Leyendo autor por ID
exports.getOne = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `cliente` WHERE `idcliente`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "No se encontró cliente!",
      });
    }

    res.status(200).json(row[0]);

  } catch (err) {
    next(err);
  }

};

// Actualizando cliente
exports.update = async (req, res, next) => {
  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `cliente` WHERE `idcliente`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "idcliente no valido",
      });
    }

    if (req.body.rut) row[0].rut = req.body.rut;

    if (req.body.nombres) row[0].nombres = req.body.nombres;

    if (req.body.apellido_paterno) row[0].apellido_paterno = req.body.apellido_paterno;

    if (req.body.apellido_materno) row[0].apellido_materno = req.body.apellido_materno;

    if (req.body.fecha_nacimiento) row[0].apellido_materno = req.body.fecha_nacimiento;

    const [update] = await db_connection.execute(
      "UPDATE `cliente` SET `rut`=?,`nombres`=?, `apellido_paterno`=?, `apellido_materno`=?, `fecha_nacimiento`=? WHERE `id`=?",
      [row[0].rut,row[0].nombres, row[0].apellido_paterno, row[0].apellido_materno, fecha_nacimiento[0], req.params.id]
    );

    if (update.affectedRows === 1) {
      return res.json({
        message: "El cliente ha sido actualizado correctamente.",
      });
    }

  } catch (err) {
    next(err);
  }

};

// Eliminando autor
exports.delete = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "DELETE FROM `cliente` WHERE `idcliente`=?",
        [req.params.id]
    );

    if (row.affectedRows === 0) {
      return res.status(404).json({
        message: "id de Cliente invalido ID (No se encontró!)",
      });
    }

    res.status(200).json({
      message: "El cliente ha sido eliminado correctamente.",
    });
    
  } catch (err) {
    next(err);
  }

};
