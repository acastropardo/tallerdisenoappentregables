const db_connection = require("../db-connection").promise();

// Leyendo autor por ID
exports.login = async (req, res, next) => {

    if (!req.body.name || !req.body.password) {
        return res.status(400).json({
          message: "Por favor ingrese todos los campos obligatorios.",
          fields: ["name","password"],
        });
      }

  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `users` WHERE `name`= ?",
        [req.body.name]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "Autenticación incorrecta!",
      });
    }

    res.status(200).json(row[0]);

  } catch (err) {
    next(err);
  }

};

