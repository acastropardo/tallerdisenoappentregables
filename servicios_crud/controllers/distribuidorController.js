const db_connection = require("../db-connection").promise();

// Insertando distribuidor
exports.insert = async (req, res, next) => {

  if (!req.body.rut || !req.body.nombre_empresa || !req.body.direccion || !req.body.telefono || !req.body.annio_vende) {
    return res.status(400).json({
      message: "Por favor ingrese todos los campos obligatorios.",
      fields: ["nombres","apellido_paterno", "apellido_materno"],
    });
  }

  try {
      
    const [rows] = await db_connection.execute(
      "INSERT INTO `distribuidor`(`rut`,`nombre_empresa`,`direccion`, `telefono`,`annio_vende` ) VALUES(?, ?, ?, ?, ?, ?)",
      [req.body.rut, req.body.nombre_empresa, req.body.direccion, req.body.telefono, req.body.annio_vende]
    );

    if (rows.affectedRows === 1) {
      return res.status(201).json({
        message: "El distribuidor ha sido insertado correctamente.",
        IDDISTRIBUIDOR: rows.insertId,
      });
    }

  } catch (err) {
    next(err);
  }
  
};

// Leyendo todos los autores
exports.getAll = async (req, res, next) => {
  try {

    const [rows] = await db_connection.execute("SELECT * FROM `distribuidor`");

    if (rows.length === 0) {
      return res.status(200).json({
        message:
          "Aún no hay registros en la tabla autor.",
      });
    }

    res.status(200).json(rows);

  } catch (err) {
    next(err);
  }

};


// Leyendo autor por ID
exports.getOne = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `distribuidor` WHERE `iddistribuidor`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "No se encontró autor!",
      });
    }

    res.status(200).json(row[0]);

  } catch (err) {
    next(err);
  }

};

// Actualizando autor
exports.update = async (req, res, next) => {
  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `distribuidor` WHERE `iddistribuidor`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "iddistribuidor no valido",
      });
    }

    if (req.body.rut) row[0].rut = req.body.rut;

    if (req.body.nombre_empresa) row[0].nombre_empresa = req.body.nombre_empresa;

    if (req.body.direccion) row[0].direccion = req.body.direccion;

    if (req.body.telefono) row[0].telefono = req.body.telefono;

    if (req.body.annio_vende) row[0].annio_vende = req.body.annio_vende;

    const [update] = await db_connection.execute(
      "UPDATE `distribuidor` SET `rut`=?, `nombre_empresa`=?, `direccion`=?, `telefono`=?, `annio_vende`=? WHERE `iddistribuidor`=?",
      [row[0].rut, row[0].nombre_empresa, row[0].direccion, row[0].telefono, row[0].annio_vende, req.params.id]
    );

    if (update.affectedRows === 1) {
      return res.json({
        message: "El distribuidor ha sido actualizado correctamente.",
      });
    }

  } catch (err) {
    next(err);
  }

};

// Eliminando autor
exports.delete = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "DELETE FROM `distribuidor` WHERE `iddistribuidor`=?",
        [req.params.id]
    );

    if (row.affectedRows === 0) {
      return res.status(404).json({
        message: "id de distribuidor invalido ID (No se encontró!)",
      });
    }

    res.status(200).json({
      message: "El autor ha sido eliminado correctamente.",
    });
    
  } catch (err) {
    next(err);
  }

};
