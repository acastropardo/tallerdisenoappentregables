const db_connection = require("../db-connection").promise();

// INSERTAR categoria
exports.insert = async (req, res, next) => {

  if (!req.body.categoria) {
    return res.status(400).json({
      message: "Porfavor llene todos los campos.",
      fields: ["categoria"],
    });
  }

  try {
      
    const [rows] = await db_connection.execute(
      "INSERT INTO `categoria`(`categoria`) VALUES(?)",
      [req.body.categoria]
    );

    if (rows.affectedRows === 1) {
      return res.status(201).json({
        message: "La categoria ha sido insertado correctamente.",
        idcategoria: rows.insertId,
      });
    }

  } catch (err) {
    next(err);
  }
  
};

// Leyendo todos los categorias
exports.getAll = async (req, res, next) => {
  try {

    const [rows] = await db_connection.execute("SELECT * FROM `categoria`");

    if (rows.length === 0) {
      return res.status(200).json({
        message:
          "No hay registros de categoria en la base de datos todavía.",
      });
    }

    res.status(200).json(rows);

  } catch (err) {
    next(err);
  }

};


// Leyendo categoria por un ID
exports.getOne = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `categoria` WHERE `idcategoria`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "Registro no encontrado!",
      });
    }

    res.status(200).json(row[0]);

  } catch (err) {
    next(err);
  }

};

// Actualicar categoria 
exports.update = async (req, res, next) => {
  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `categoria` WHERE `idcategoria`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "ID de categoria no válido.",
      });
    }

    if (req.body.categoria) row[0].categoria = req.body.categoria;

    const [update] = await db_connection.execute(
      "UPDATE `categoria` SET `categoria`=? WHERE `idcategoria`=?",
      [row[0].categoria, req.params.id]
    );

    if (update.affectedRows === 1) {
      return res.json({
        message: "El categoria ha sido actualizado correctamente.",
      });
    }

  } catch (err) {
    next(err);
  }

};

// Borrando categoria
exports.delete = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "DELETE FROM `categoria` WHERE `idcategoria`=?",
        [req.params.id]
    );

    if (row.affectedRows === 0) {
      return res.status(404).json({
        message: "ID categoria no válido (No se encontró!)",
      });
    }

    res.status(200).json({
      message: "El categoria ha sido eliminado correctamente.",
    });
    
  } catch (err) {
    next(err);
  }

};
