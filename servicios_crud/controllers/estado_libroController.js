const db_connection = require("../db-connection").promise();

// INSERTAR estado_libro
exports.insert = async (req, res, next) => {

  if (!req.body.estado_libro) {
    return res.status(400).json({
      message: "Porfavor llene todos los campos.",
      fields: ["estado_libro"],
    });
  }

  try {
      
    const [rows] = await db_connection.execute(
      "INSERT INTO `estado_libro`(`estado_libro`) VALUES(?)",
      [req.body.estado_libro]
    );

    if (rows.affectedRows === 1) {
      return res.status(201).json({
        message: "El estado_libro ha sido insertado correctamente.",
        idestado_libro: rows.insertId,
      });
    }

  } catch (err) {
    next(err);
  }
  
};

// Leyendo todos los estado_libros
exports.getAll = async (req, res, next) => {
  try {

    const [rows] = await db_connection.execute("SELECT * FROM `estado_libro`");

    if (rows.length === 0) {
      return res.status(200).json({
        message:
          "No hay registros de estado_libro en la base de datos todavía.",
      });
    }

    res.status(200).json(rows);

  } catch (err) {
    next(err);
  }

};


// Leyendo estado_libro por un ID
exports.getOne = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `estado_libro` WHERE `idestado_libro`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "Registro no encontrado!",
      });
    }

    res.status(200).json(row[0]);

  } catch (err) {
    next(err);
  }

};

// Actualicar estado_libro 
exports.update = async (req, res, next) => {
  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `estado_libro` WHERE `idestado_libro`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "ID de estado_libro no válido.",
      });
    }

    if (req.body.estado_libro) row[0].estado_libro = req.body.estado_libro;

    const [update] = await db_connection.execute(
      "UPDATE `estado_libro` SET `estado_libro`=? WHERE `idestado_libro`=?",
      [row[0].estado_libro, req.params.id]
    );

    if (update.affectedRows === 1) {
      return res.json({
        message: "El estado_libro ha sido actualizado correctamente.",
      });
    }

  } catch (err) {
    next(err);
  }

};

// Borrando estado_libro
exports.delete = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "DELETE FROM `estado_libro` WHERE `idestado_libro`=?",
        [req.params.id]
    );

    if (row.affectedRows === 0) {
      return res.status(404).json({
        message: "ID estado_libro no válido (No se encontró!)",
      });
    }

    res.status(200).json({
      message: "El estado_libro ha sido eliminado correctamente.",
    });
    
  } catch (err) {
    next(err);
  }

};
