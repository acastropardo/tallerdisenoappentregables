const db_connection = require("../db-connection").promise();

// Insertando libro
exports.insert = async (req, res, next) => {

  if (!req.body.numero_serie || !req.body.titulo || !req.body.no_paginas || !req.body.precio_referencia || !req.body.annio || !req.body.cantidad_stock_venta || !req.body.cantidad_stock_prestamo || !req.body.categoria_idcategoria || !req.body.autor_idautor || !req.body.estado_libro_idestado_libro || !req.body.idioma_ididioma || !req.body.editorial_ideditorial ) {
    return res.status(400).json({
      message: "Por favor ingrese todos los campos obligatorios.",
      fields: ["numero_serie","titulo", "no_paginas","precio_referencia","annio,cantidad_stock_venta","cantidad_stock_prestamo","categoria_idcategoria","autor_idautor","estado_libro_idestado_libro","idioma_ididioma","editorial_ideditorial"]
    });
  }

  try {
      
    const [rows] = await db_connection.execute(
      "INSERT INTO `libro`(`numero_serie`,`titulo`,`no_paginas`,`precio_referencia`,`annio`,`cantidad_stock_venta`,`cantidad_stock_prestamo`,`categoria_idcategoria`,`autor_idautor`,`estado_libro_idestado_libro`,`idioma_ididioma`,`editorial_ideditorial` ) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
      [req.body.numero_serie, req.body.titulo, req.body.no_paginas, req.body.precio_referencia, req.body.annio, req.body.cantidad_stock_venta, req.body.cantidad_stock_prestamo, req.body.categoria_idcategoria, req.body.autor_idautor, req.body.estado_libro_idestado_libro, req.body.idioma_ididioma, req.body.editorial_ideditorial]
    );

    if (rows.affectedRows === 1) {
      return res.status(201).json({
        message: "El libro ha sido insertado correctamente.",
        idlibro: rows.insertId,
      });
    }

  } catch (err) {
    next(err);
  }
  
};

// Leyendo todos los autores
exports.getAll = async (req, res, next) => {
  try {

    const [rows] = await db_connection.execute("SELECT * FROM `libro`");

    if (rows.length === 0) {
      return res.status(200).json({
        message:
          "Aún no hay registros en la tabla libro.",
      });
    }

    res.status(200).json(rows);

  } catch (err) {
    next(err);
  }

};


// Leyendo libro por ID
exports.getOne = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `libro` WHERE `idlibro`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "No se encontró libro!",
      });
    }

    res.status(200).json(row[0]);

  } catch (err) {
    next(err);
  }

};

// Actualizando libro
exports.update = async (req, res, next) => {
  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `libro` WHERE `idlibro`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "idlibro no valido",
      });
    }

    if (req.body.numero_serie) row[0].numero_serie = req.body.numero_serie;
    if (req.body.titulo) row[0].titulo = req.body.titulo;
    if (req.body.no_paginas) row[0].no_paginas = req.body.no_paginas;
    if (req.body.precio_referencia) row[0].precio_referencia = req.body.precio_referencia;
    if (req.body.annio) row[0].annio = req.body.annio;
    if (req.body.cantidad_stock_prestamo) row[0].cantidad_stock_prestamo = req.body.cantidad_stock_prestamo;
    if (req.body.categoria_idcategoria) row[0].categoria_idcategoria = req.body.categoria_idcategoria;
    if (req.body.autor_idautor) row[0].autor_idautor = req.body.autor_idautor;
    if (req.body.estado_libro_idestado_libro) row[0].estado_libro_idestado_libro = req.body.estado_libro_idestado_libro;
    if (req.body.idioma_ididioma) row[0].idioma_ididioma = req.body.idioma_ididioma;
    if (req.body.editorial_ideditorial) row[0].editorial_ideditorial = req.body.editorial_ideditorial;
  
    const [update] = await db_connection.execute(
      "UPDATE `libro` SET `numero_serie`=?, `titulo`=?, `no_paginas`=?,`precio_referencia`=?,`annio`=?,`cantidad_stock_venta`=?,`cantidad_stock_prestamo`=?,`categoria_idcategoria`=?,`autor_idautor`=?,`estado_libro_idestado_libro`=?,`idioma_ididioma`=?,`editorial_ideditorial`=?  WHERE `idlibro`=?",
      [row[0].numero_serie, row[0].titulo, row[0].no_paginas, req.precio_referencia[0], req.annio[0], req.cantidad_stock_venta[0], req.cantidad_stock_prestamo[0], req.categoria_idcategoria[0], req.autor_idautor[0],req.estado_libro_idestado_libro[0],req.idioma_ididioma[0].req.editorial_ideditorial[0],req.params.id]
    );

    if (update.affectedRows === 1) {
      return res.json({
        message: "El libro ha sido actualizado correctamente.",
      });
    }

  } catch (err) {
    next(err);
  }

};

// Eliminando autor
exports.delete = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "DELETE FROM `libro` WHERE `idlibro`=?",
        [req.params.id]
    );

    if (row.affectedRows === 0) {
      return res.status(404).json({
        message: "id de libro invalido ID (No se encontró!)",
      });
    }

    res.status(200).json({
      message: "El libro ha sido eliminado correctamente.",
    });
    
  } catch (err) {
    next(err);
  }

};
