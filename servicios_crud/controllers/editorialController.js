const db_connection = require("../db-connection").promise();

// INSERTAR editorial
exports.insert = async (req, res, next) => {

  if (!req.body.editorial) {
    return res.status(400).json({
      message: "Porfavor llene todos los campos.",
      fields: ["editorial"],
    });
  }

  try {
      
    const [rows] = await db_connection.execute(
      "INSERT INTO `editorial`(`editorial`) VALUES(?)",
      [req.body.editorial]
    );

    if (rows.affectedRows === 1) {
      return res.status(201).json({
        message: "El editorial ha sido insertado correctamente.",
        ideditorial: rows.insertId,
      });
    }

  } catch (err) {
    next(err);
  }
  
};

// Leyendo todos los editorials
exports.getAll = async (req, res, next) => {
  try {

    const [rows] = await db_connection.execute("SELECT * FROM `editorial`");

    if (rows.length === 0) {
      return res.status(200).json({
        message:
          "No hay registros de editorial en la base de datos todavía.",
      });
    }

    res.status(200).json(rows);

  } catch (err) {
    next(err);
  }

};


// Leyendo editorial por un ID
exports.getOne = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `editorial` WHERE `ideditorial`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "Registro no encontrado!",
      });
    }

    res.status(200).json(row[0]);

  } catch (err) {
    next(err);
  }

};

// Actualicar editorial 
exports.update = async (req, res, next) => {
  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `editorial` WHERE `ideditorial`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "ID de editorial no válido.",
      });
    }

    if (req.body.editorial) row[0].editorial = req.body.editorial;

    const [update] = await db_connection.execute(
      "UPDATE `editorial` SET `editorial`=? WHERE `ideditorial`=?",
      [row[0].editorial, req.params.id]
    );

    if (update.affectedRows === 1) {
      return res.json({
        message: "El editorial ha sido actualizado correctamente.",
      });
    }

  } catch (err) {
    next(err);
  }

};

// Borrando editorial
exports.delete = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "DELETE FROM `editorial` WHERE `ideditorial`=?",
        [req.params.id]
    );

    if (row.affectedRows === 0) {
      return res.status(404).json({
        message: "ID editorial no válido (No se encontró!)",
      });
    }

    res.status(200).json({
      message: "El editorial ha sido eliminado correctamente.",
    });
    
  } catch (err) {
    next(err);
  }

};
