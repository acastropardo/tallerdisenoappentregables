const db_connection = require("../db-connection").promise();

// Insertando detalle_arriendo
exports.insert = async (req, res, next) => {

  if (!req.body.arriendo_idarriendo || !req.body.libro_idlibro || !req.body.libro_numero_serie ) {
    return res.status(400).json({
      message: "Por favor ingrese todos los campos obligatorios.",
      fields: ["arriendo_idarriendo","libro_idlibro", "libro_numero_serie"]
    });
  }

  try {
      
    const [rows] = await db_connection.execute(
      "INSERT INTO `detalle_arriendo`(`arriendo_idarriendo`,`libro_idlibro`,`libro_numero_serie`) VALUES(?, ?, ?)",
      [req.body.arriendo_idarriendo, req.body.libro_idlibro, req.body.libro_numero_serie]
    );

    if (rows.affectedRows === 1) {
      return res.status(201).json({
        message: "El detalle_arriendo ha sido insertado correctamente.",
        iddetalle_arriendo: rows.insertId,
      });
    }

  } catch (err) {
    next(err);
  }
  
};

// Leyendo todos los detalle_arriendos
exports.getAll = async (req, res, next) => {
  try {

    const [rows] = await db_connection.execute("SELECT * FROM `detalle_arriendo`");

    if (rows.length === 0) {
      return res.status(200).json({
        message:
          "Aún no hay registros en la tabla detalle_arriendo.",
      });
    }

    res.status(200).json(rows);

  } catch (err) {
    next(err);
  }

};

// Leyendo detalle_arriendo por ID
exports.getOne = async (req, res, next) => {

    try {
  
      const [row] = await db_connection.execute(
          "SELECT * FROM `detalle_arriendo` WHERE `iddetalle_arriendo`=?",
          [req.params.id]
      );
  
      if (row.length === 0) {
        return res.status(404).json({
          message: "No se encontró arriendo!",
        });
      }
  
      res.status(200).json(row[0]);
  
    } catch (err) {
      next(err);
    }
  
  };

// Leyendo detalle_arriendo por ID de arriendo
exports.getDetaArriendo = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `detalle_arriendo` WHERE `arriendo_idarriendo`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "No se encontró arriendo!",
      });
    }

    res.status(200).json(row[0]);

  } catch (err) {
    next(err);
  }

};

// Actualizando detalle_arriendo
exports.update = async (req, res, next) => {
  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `detalle_arriendo` WHERE `iddetalle_arriendo`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "iddetalle_arriendo no valido",
      });
    }

    if (req.body.arriendo_idarriendo) row[0].arriendo_idarriendo = req.body.arriendo_idarriendo;
    if (req.body.libro_idlibro) row[0].libro_idlibro = req.body.libro_idlibro;
    if (req.body.libro_numero_serie) row[0].libro_numero_serie = req.body.libro_numero_serie;


  
    const [update] = await db_connection.execute(
      "UPDATE `detalle_arriendo` SET `arriendo_idarriendo`=?, `libro_idlibro`=?, `libro_numero_serie`=?  WHERE `iddetalle_arriendo`=?",
      [row[0].arriendo_idarriendo, row[0].libro_idlibro, row[0].libro_numero_serie, req.params.id]
    );

    if (update.affectedRows === 1) {
      return res.json({
        message: "El detalle_arriendo ha sido actualizado correctamente.",
      });
    }

  } catch (err) {
    next(err);
  }

};

// Eliminando detalle_arriendo
exports.delete = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "DELETE FROM `detalle_arriendo` WHERE `iddetalle_arriendo`=?",
        [req.params.id]
    );

    if (row.affectedRows === 0) {
      return res.status(404).json({
        message: "id de detalle_arriendo invalido ID (No se encontró!)",
      });
    }

    res.status(200).json({
      message: "El detalle_arriendo ha sido eliminado correctamente.",
    });
    
  } catch (err) {
    next(err);
  }

};
