const db_connection = require("../db-connection").promise();

// INSERTAR IDIOMA
exports.insert = async (req, res, next) => {

  if (!req.body.idioma) {
    return res.status(400).json({
      message: "Porfavor llene todos los campos.",
      fields: ["idioma"],
    });
  }

  try {
      
    const [rows] = await db_connection.execute(
      "INSERT INTO `idioma`(`idioma`) VALUES(?)",
      [req.body.idioma]
    );

    if (rows.affectedRows === 1) {
      return res.status(201).json({
        message: "El idioma ha sido insertado correctamente.",
        ididioma: rows.insertId,
      });
    }

  } catch (err) {
    next(err);
  }
  
};

// Leyendo todos los idiomas
exports.getAll = async (req, res, next) => {
  try {

    const [rows] = await db_connection.execute("SELECT * FROM `idioma`");

    if (rows.length === 0) {
      return res.status(200).json({
        message:
          "No hay registros de idioma en la base de datos todavía.",
      });
    }

    res.status(200).json(rows);

  } catch (err) {
    next(err);
  }

};


// Leyendo idioma por un ID
exports.getOne = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `idioma` WHERE `ididioma`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "Registro no encontrado!",
      });
    }

    res.status(200).json(row[0]);

  } catch (err) {
    next(err);
  }

};

// Actualicar idioma 
exports.update = async (req, res, next) => {
  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `idioma` WHERE `ididioma`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "ID de idioma no válido.",
      });
    }

    if (req.body.idioma) row[0].idioma = req.body.idioma;

    const [update] = await db_connection.execute(
      "UPDATE `idioma` SET `idioma`=? WHERE `ididioma`=?",
      [row[0].idioma, req.params.id]
    );

    if (update.affectedRows === 1) {
      return res.json({
        message: "El idioma ha sido actualizado correctamente.",
      });
    }

  } catch (err) {
    next(err);
  }

};

// Borrando Idioma
exports.delete = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "DELETE FROM `idioma` WHERE `ididioma`=?",
        [req.params.id]
    );

    if (row.affectedRows === 0) {
      return res.status(404).json({
        message: "ID idioma no válido (No se encontró!)",
      });
    }

    res.status(200).json({
      message: "El idioma ha sido eliminado correctamente.",
    });
    
  } catch (err) {
    next(err);
  }

};
