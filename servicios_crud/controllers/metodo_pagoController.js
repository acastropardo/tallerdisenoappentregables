const db_connection = require("../db-connection").promise();

// INSERTAR metodo_pago
exports.insert = async (req, res, next) => {

  if (!req.body.metodo_pago) {
    return res.status(400).json({
      message: "Porfavor llene todos los campos.",
      fields: ["metodo_pago"],
    });
  }

  try {
      
    const [rows] = await db_connection.execute(
      "INSERT INTO `metodo_pago`(`metodo_pago`) VALUES(?)",
      [req.body.metodo_pago]
    );

    if (rows.affectedRows === 1) {
      return res.status(201).json({
        message: "El metodo_pago ha sido insertado correctamente.",
        idmetodo_pago: rows.insertId,
      });
    }

  } catch (err) {
    next(err);
  }
  
};

// Leyendo todos los metodo_pagos
exports.getAll = async (req, res, next) => {
  try {

    const [rows] = await db_connection.execute("SELECT * FROM `metodo_pago`");

    if (rows.length === 0) {
      return res.status(200).json({
        message:
          "No hay registros de metodo_pago en la base de datos todavía.",
      });
    }

    res.status(200).json(rows);

  } catch (err) {
    next(err);
  }

};


// Leyendo metodo_pago por un ID
exports.getOne = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `metodo_pago` WHERE `idmetodo_pago`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "Registro no encontrado!",
      });
    }

    res.status(200).json(row[0]);

  } catch (err) {
    next(err);
  }

};

// Actualicar metodo_pago 
exports.update = async (req, res, next) => {
  try {

    const [row] = await db_connection.execute(
        "SELECT * FROM `metodo_pago` WHERE `idmetodo_pago`=?",
        [req.params.id]
    );

    if (row.length === 0) {
      return res.status(404).json({
        message: "ID de metodo_pago no válido.",
      });
    }

    if (req.body.metodo_pago) row[0].metodo_pago = req.body.metodo_pago;

    const [update] = await db_connection.execute(
      "UPDATE `metodo_pago` SET `metodo_pago`=? WHERE `idmetodo_pago`=?",
      [row[0].metodo_pago, req.params.id]
    );

    if (update.affectedRows === 1) {
      return res.json({
        message: "El metodo_pago ha sido actualizado correctamente.",
      });
    }

  } catch (err) {
    next(err);
  }

};

// Borrando metodo_pago
exports.delete = async (req, res, next) => {

  try {

    const [row] = await db_connection.execute(
        "DELETE FROM `metodo_pago` WHERE `idmetodo_pago`=?",
        [req.params.id]
    );

    if (row.affectedRows === 0) {
      return res.status(404).json({
        message: "ID metodo_pago no válido (No se encontró!)",
      });
    }

    res.status(200).json({
      message: "El metodo_pago ha sido eliminado correctamente.",
    });
    
  } catch (err) {
    next(err);
  }

};
