-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`categoria` (
  `idcategoria` INT NOT NULL AUTO_INCREMENT,
  `categoria` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`idcategoria`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`autor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`autor` (
  `idautor` INT NOT NULL AUTO_INCREMENT,
  `nombres` VARCHAR(100) NOT NULL,
  `apellido_paterno` VARCHAR(100) NOT NULL,
  `apellido_materno` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`idautor`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`estado_libro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`estado_libro` (
  `idestado_libro` INT NOT NULL AUTO_INCREMENT,
  `estado_libro` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`idestado_libro`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`idioma`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`idioma` (
  `ididioma` INT NOT NULL AUTO_INCREMENT,
  `idioma` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ididioma`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`editorial`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`editorial` (
  `ideditorial` INT NOT NULL AUTO_INCREMENT,
  `editorial` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ideditorial`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`libro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`libro` (
  `idlibro` INT NOT NULL AUTO_INCREMENT,
  `numero_serie` VARCHAR(45) NOT NULL,
  `titulo` VARCHAR(100) NOT NULL,
  `no_paginas` INT NOT NULL,
  `precio_referencia` INT NOT NULL,
  `annio` DATE NOT NULL,
  `categoria_idcategoria` INT NOT NULL,
  `autor_idautor` INT NOT NULL,
  `estado_libro_idestado_libro` INT NOT NULL,
  `idioma_ididioma` INT NOT NULL,
  `editorial_ideditorial` INT NOT NULL,
  PRIMARY KEY (`idlibro`, `numero_serie`),
  INDEX `fk_libro_categoria_idx` (`categoria_idcategoria` ASC),
  INDEX `fk_libro_autor1_idx` (`autor_idautor` ASC),
  INDEX `fk_libro_estado_libro1_idx` (`estado_libro_idestado_libro` ASC),
  INDEX `fk_libro_idioma1_idx` (`idioma_ididioma` ASC),
  INDEX `fk_libro_editorial1_idx` (`editorial_ideditorial` ASC),
  CONSTRAINT `fk_libro_categoria`
    FOREIGN KEY (`categoria_idcategoria`)
    REFERENCES `mydb`.`categoria` (`idcategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_libro_autor1`
    FOREIGN KEY (`autor_idautor`)
    REFERENCES `mydb`.`autor` (`idautor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_libro_estado_libro1`
    FOREIGN KEY (`estado_libro_idestado_libro`)
    REFERENCES `mydb`.`estado_libro` (`idestado_libro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_libro_idioma1`
    FOREIGN KEY (`idioma_ididioma`)
    REFERENCES `mydb`.`idioma` (`ididioma`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_libro_editorial1`
    FOREIGN KEY (`editorial_ideditorial`)
    REFERENCES `mydb`.`editorial` (`ideditorial`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`distribuidor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`distribuidor` (
  `iddistribuidor` INT NOT NULL AUTO_INCREMENT,
  `rut` CHAR(12) NOT NULL,
  `nombre_empresa` VARCHAR(45) NOT NULL,
  `direccion` VARCHAR(100) NOT NULL,
  `telefono` VARCHAR(12) NOT NULL,
  `annio_vende` CHAR(4) NOT NULL,
  PRIMARY KEY (`iddistribuidor`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`telefono`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`telefono` (
  `idtelefono` INT NOT NULL AUTO_INCREMENT,
  `numero` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idtelefono`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`direccion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`direccion` (
  `iddireccion` INT NOT NULL AUTO_INCREMENT,
  `direccion` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`iddireccion`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`correo_electronico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`correo_electronico` (
  `idcorreo_electronico` INT NOT NULL AUTO_INCREMENT,
  `correo_electronico` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`idcorreo_electronico`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`cliente` (
  `idcliente` INT NOT NULL AUTO_INCREMENT,
  `rut` CHAR(12) NOT NULL,
  `apellido_paterno` VARCHAR(45) NOT NULL,
  `apellido_materno` VARCHAR(45) NOT NULL,
  `fecha_nacimiento` DATE NOT NULL,
  `telefono_idtelefono` INT NOT NULL,
  `direccion_iddireccion` INT NOT NULL,
  `correo_electronico_idcorreo_electronico` INT NOT NULL,
  PRIMARY KEY (`idcliente`),
  INDEX `fk_cliente_telefono1_idx` (`telefono_idtelefono` ASC),
  INDEX `fk_cliente_direccion1_idx` (`direccion_iddireccion` ASC),
  INDEX `fk_cliente_correo_electronico1_idx` (`correo_electronico_idcorreo_electronico` ASC),
  CONSTRAINT `fk_cliente_telefono1`
    FOREIGN KEY (`telefono_idtelefono`)
    REFERENCES `mydb`.`telefono` (`idtelefono`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cliente_direccion1`
    FOREIGN KEY (`direccion_iddireccion`)
    REFERENCES `mydb`.`direccion` (`iddireccion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cliente_correo_electronico1`
    FOREIGN KEY (`correo_electronico_idcorreo_electronico`)
    REFERENCES `mydb`.`correo_electronico` (`idcorreo_electronico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`trabajador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`trabajador` (
  `idtrabajador` INT NOT NULL AUTO_INCREMENT,
  `rut` CHAR(12) NOT NULL,
  `apellido_paterno` VARCHAR(45) NOT NULL,
  `apellido_materno` VARCHAR(45) NOT NULL,
  `fecha_contrato` DATE NOT NULL,
  `telefono_idtelefono` INT NOT NULL,
  `direccion_iddireccion` INT NOT NULL,
  `correo_electronico_idcorreo_electronico1` INT NOT NULL,
  PRIMARY KEY (`idtrabajador`),
  INDEX `fk_trabajador_telefono1_idx` (`telefono_idtelefono` ASC),
  INDEX `fk_trabajador_direccion1_idx` (`direccion_iddireccion` ASC),
  INDEX `fk_trabajador_correo_electronico1_idx` (`correo_electronico_idcorreo_electronico1` ASC),
  CONSTRAINT `fk_trabajador_telefono1`
    FOREIGN KEY (`telefono_idtelefono`)
    REFERENCES `mydb`.`telefono` (`idtelefono`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_trabajador_direccion1`
    FOREIGN KEY (`direccion_iddireccion`)
    REFERENCES `mydb`.`direccion` (`iddireccion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_trabajador_correo_electronico1`
    FOREIGN KEY (`correo_electronico_idcorreo_electronico1`)
    REFERENCES `mydb`.`correo_electronico` (`idcorreo_electronico`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`metodo_pago`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`metodo_pago` (
  `idmetodo_pago` INT NOT NULL AUTO_INCREMENT,
  `metodo_pago` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idmetodo_pago`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`factura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`factura` (
  `idfactura` INT NOT NULL AUTO_INCREMENT,
  `folio` INT NOT NULL,
  `precio_neto` INT NOT NULL,
  `precio_iva` INT NOT NULL,
  `costo_iva` INT NOT NULL,
  `fecha_compra` DATE NOT NULL,
  `hora_compra` TIME NOT NULL,
  `metodo_pago_idmetodo_pago` INT NOT NULL,
  `distribuidor_iddistribuidor` INT NOT NULL,
  PRIMARY KEY (`idfactura`),
  INDEX `fk_factura_metodo_pago1_idx` (`metodo_pago_idmetodo_pago` ASC),
  INDEX `fk_factura_distribuidor1_idx` (`distribuidor_iddistribuidor` ASC),
  CONSTRAINT `fk_factura_metodo_pago1`
    FOREIGN KEY (`metodo_pago_idmetodo_pago`)
    REFERENCES `mydb`.`metodo_pago` (`idmetodo_pago`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_factura_distribuidor1`
    FOREIGN KEY (`distribuidor_iddistribuidor`)
    REFERENCES `mydb`.`distribuidor` (`iddistribuidor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`compra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`compra` (
  `idcompra` INT NOT NULL AUTO_INCREMENT,
  `factura_idfactura` INT NOT NULL,
  `distribuidor_iddistribuidor` INT NOT NULL,
  `factura_idfactura1` INT NOT NULL,
  PRIMARY KEY (`idcompra`),
  INDEX `fk_compra_factura1_idx` (`factura_idfactura` ASC),
  INDEX `fk_compra_distribuidor1_idx` (`distribuidor_iddistribuidor` ASC),
  INDEX `fk_compra_factura2_idx` (`factura_idfactura1` ASC),
  CONSTRAINT `fk_compra_factura1`
    FOREIGN KEY (`factura_idfactura`)
    REFERENCES `mydb`.`factura` (`idfactura`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_compra_distribuidor1`
    FOREIGN KEY (`distribuidor_iddistribuidor`)
    REFERENCES `mydb`.`distribuidor` (`iddistribuidor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_compra_factura2`
    FOREIGN KEY (`factura_idfactura1`)
    REFERENCES `mydb`.`factura` (`idfactura`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`boleta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`boleta` (
  `idboleta` INT NOT NULL AUTO_INCREMENT,
  `folio` INT NOT NULL,
  `precio_neto` INT NOT NULL,
  `precio_iva` INT NOT NULL,
  `costo_iva` INT NOT NULL,
  `fecha_venta` DATE NOT NULL,
  `hora_venta` TIME NOT NULL,
  `cliente_idcliente` INT NOT NULL,
  `trabajador_idtrabajador` INT NOT NULL,
  PRIMARY KEY (`idboleta`),
  INDEX `fk_boleta_cliente1_idx` (`cliente_idcliente` ASC),
  INDEX `fk_boleta_trabajador1_idx` (`trabajador_idtrabajador` ASC),
  CONSTRAINT `fk_boleta_cliente1`
    FOREIGN KEY (`cliente_idcliente`)
    REFERENCES `mydb`.`cliente` (`idcliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_boleta_trabajador1`
    FOREIGN KEY (`trabajador_idtrabajador`)
    REFERENCES `mydb`.`trabajador` (`idtrabajador`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`venta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`venta` (
  `idventa` INT NOT NULL AUTO_INCREMENT,
  `cliente_idcliente` INT NOT NULL,
  `trabajador_idtrabajador` INT NOT NULL,
  `boleta_idboleta` INT NOT NULL,
  PRIMARY KEY (`idventa`),
  INDEX `fk_venta_cliente1_idx` (`cliente_idcliente` ASC),
  INDEX `fk_venta_trabajador1_idx` (`trabajador_idtrabajador` ASC),
  INDEX `fk_venta_boleta1_idx` (`boleta_idboleta` ASC),
  CONSTRAINT `fk_venta_cliente1`
    FOREIGN KEY (`cliente_idcliente`)
    REFERENCES `mydb`.`cliente` (`idcliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_venta_trabajador1`
    FOREIGN KEY (`trabajador_idtrabajador`)
    REFERENCES `mydb`.`trabajador` (`idtrabajador`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_venta_boleta1`
    FOREIGN KEY (`boleta_idboleta`)
    REFERENCES `mydb`.`boleta` (`idboleta`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`detalle_compra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`detalle_compra` (
  `iddetalle_compra` INT NOT NULL AUTO_INCREMENT,
  `compra_idcompra` INT NOT NULL,
  `libro_idlibro` INT NOT NULL,
  `libro_numero_serie` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`iddetalle_compra`),
  INDEX `fk_detalle_compra_compra1_idx` (`compra_idcompra` ASC),
  INDEX `fk_detalle_compra_libro1_idx` (`libro_idlibro` ASC, `libro_numero_serie` ASC),
  CONSTRAINT `fk_detalle_compra_compra1`
    FOREIGN KEY (`compra_idcompra`)
    REFERENCES `mydb`.`compra` (`idcompra`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_compra_libro1`
    FOREIGN KEY (`libro_idlibro` , `libro_numero_serie`)
    REFERENCES `mydb`.`libro` (`idlibro` , `numero_serie`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`detalle_venta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`detalle_venta` (
  `iddetalle_venta` INT NOT NULL,
  `venta_idventa` INT NOT NULL,
  `libro_idlibro` INT NOT NULL,
  `libro_numero_serie` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`iddetalle_venta`),
  INDEX `fk_detalle_venta_venta1_idx` (`venta_idventa` ASC),
  INDEX `fk_detalle_venta_libro1_idx` (`libro_idlibro` ASC, `libro_numero_serie` ASC),
  CONSTRAINT `fk_detalle_venta_venta1`
    FOREIGN KEY (`venta_idventa`)
    REFERENCES `mydb`.`venta` (`idventa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_venta_libro1`
    FOREIGN KEY (`libro_idlibro` , `libro_numero_serie`)
    REFERENCES `mydb`.`libro` (`idlibro` , `numero_serie`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`arriendo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`arriendo` (
  `idarriendo` INT NOT NULL AUTO_INCREMENT,
  `costo_total` INT NOT NULL,
  `fecha_arriendo` DATE NOT NULL,
  `fecha_devolucion_estimada` DATE NOT NULL,
  `fecha_entrega_real` DATE NULL,
  `dias_retraso` INT NULL,
  `multa` INT NULL,
  `costo_arriendo` INT NOT NULL,
  `cliente_idcliente` INT NOT NULL,
  `trabajador_idtrabajador` INT NOT NULL,
  PRIMARY KEY (`idarriendo`),
  INDEX `fk_arriendo_cliente1_idx` (`cliente_idcliente` ASC),
  INDEX `fk_arriendo_trabajador1_idx` (`trabajador_idtrabajador` ASC),
  CONSTRAINT `fk_arriendo_cliente1`
    FOREIGN KEY (`cliente_idcliente`)
    REFERENCES `mydb`.`cliente` (`idcliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_arriendo_trabajador1`
    FOREIGN KEY (`trabajador_idtrabajador`)
    REFERENCES `mydb`.`trabajador` (`idtrabajador`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`detalle_arriendo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`detalle_arriendo` (
  `iddetalle_arriendo` INT NOT NULL AUTO_INCREMENT,
  `arriendo_idarriendo` INT NOT NULL,
  `libro_idlibro` INT NOT NULL,
  `libro_numero_serie` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`iddetalle_arriendo`),
  INDEX `fk_detalle_arriendo_arriendo1_idx` (`arriendo_idarriendo` ASC),
  INDEX `fk_detalle_arriendo_libro1_idx` (`libro_idlibro` ASC, `libro_numero_serie` ASC),
  CONSTRAINT `fk_detalle_arriendo_arriendo1`
    FOREIGN KEY (`arriendo_idarriendo`)
    REFERENCES `mydb`.`arriendo` (`idarriendo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_arriendo_libro1`
    FOREIGN KEY (`libro_idlibro` , `libro_numero_serie`)
    REFERENCES `mydb`.`libro` (`idlibro` , `numero_serie`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
