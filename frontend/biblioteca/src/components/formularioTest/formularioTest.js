import React, { Component, useState } from 'react';
import "@ui5/webcomponents/dist/Button";
import "@ui5/webcomponents/dist/Input.js";
import "@ui5/webcomponents/dist/Icon.js";
import '@ui5/webcomponents-icons/dist/json-imports/Icons.js';
import { ProductSwitch } from '@ui5/webcomponents-react';
import { FlexBox } from '@ui5/webcomponents-react';
import { Form } from '@ui5/webcomponents-react/dist/Form';
import { FormGroup } from '@ui5/webcomponents-react/dist/FormGroup';
import { FormItem } from '@ui5/webcomponents-react/dist/FormItem';
import { Input } from '@ui5/webcomponents-react/dist/Input';
import { InputType } from '@ui5/webcomponents-react/dist/InputType';
import { Label } from '@ui5/webcomponents-react/dist/Label';
import CheckBox from "@ui5/webcomponents/dist/CheckBox";
import Select from "@ui5/webcomponents/dist/Select";
import Option from "@ui5/webcomponents/dist/Option";
import TextArea from "@ui5/webcomponents/dist/TextArea";

export class FormularioTest extends Component {
    static displayName = FormularioTest.name;
    
    constructor() {
        super();
    }
    render() {
        return(
        <Form title="Test Form">
            <FormItem label="Sole Form Item">
                <Input />
            </FormItem>
            <FormGroup title="Personal Data">
                <FormItem label="Name">
                    <Input />
                </FormItem>
                <FormItem label={<Label>Address</Label>}>
                    <Input />
                </FormItem>
                <FormItem label="Country">

                </FormItem>
                <FormItem label="Additional Comment">

                </FormItem>
                <FormItem label="Home address">
                    
                </FormItem>
            </FormGroup>
            <FormGroup title="Company Data">
                <FormItem label="Company Name">
                    <Input />
                </FormItem>
                <FormItem label="Company Address">
                    <Input />
                </FormItem>
                <FormItem label="Company City">
                    <Input />
                </FormItem>
                <FormItem label="Company Country">
                    <Input />
                </FormItem>
                <FormItem label="Number of Employees">
                    <Input
                        disabled
                        type="Number"
                        value="5000"
                    />
                </FormItem>
                <FormItem label="Member of Partner Network">

                </FormItem>
            </FormGroup>
            <FormGroup title="Marketing Data">
                <FormItem label="Email">
                    <Input type="Email" />
                </FormItem>
                <FormItem label="Company Email">
                    <Input type="Email" />
                </FormItem>
                <FormItem label="I want to receive the newsletter">
                    
                </FormItem>
            </FormGroup>
        </Form>

        );
    }
}

