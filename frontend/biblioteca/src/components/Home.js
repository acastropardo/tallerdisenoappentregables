import React, { Component, useState } from 'react';

import Login from "./LoginView/LoginApp"
import Form from "react-bootstrap/Form";
import "@ui5/webcomponents/dist/Button";


export class Home extends Component {
  static displayName = Home.name;


    render() {
        return (
            <div className="Login">
                <Form >
                    <Form.Group size="lg" controlId="email">
                        <Form.Label>Email</Form.Label>
                        <Form.Control
                            autoFocus
                            type="email"
                            onChange={(e) => ""}
                        />
                    </Form.Group>
                    <Form.Group size="lg" controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            onChange={(e) => ""}
                        />
                    </Form.Group>
                    <ui5-button  block size="lg" type="submit" >
                        Login
        </ui5-button>
                </Form>
                <Login/>
            </div>
        );
    }
}
