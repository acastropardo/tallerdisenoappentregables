import React, { Component, useState } from 'react';
import LoginApp from './LoginApp';
import "@ui5/webcomponents/dist/Button";
import "@ui5/webcomponents/dist/Input.js";
import "@ui5/webcomponents/dist/Icon.js";
import '@ui5/webcomponents-icons/dist/json-imports/Icons.js';
import { ProductSwitch } from '@ui5/webcomponents-react';
import { FlexBox } from '@ui5/webcomponents-react';



export class Login extends Component {
    static displayName = Login.name;
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            error: '',
        };

        this.handlePassChange = this.handlePassChange.bind(this);
        this.handleUserChange = this.handleUserChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.dismissError = this.dismissError.bind(this);

    }

    dismissError() {
        this.setState({ error: '' });
    }


    handleSubmit(evt) {
        evt.preventDefault();

        if (!this.state.username) {
            return this.setState({ error: 'Username is required' });
        }

        if (!this.state.password) {
            return this.setState({ error: 'Password is required' });
        }

        console.log(this.state.username);

        if (this.state.username && this.state.password) {
            fetch('http://localhost:80', {
                method: 'post',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    "CLASE": "ZCL_RESERVA",
                    "ENTRADA": "{\"UNAME\":\"" + this.state.username.v + "\",\"PASSWORD\":\"" + this.state.password+"\"}",
                    "METODO": "VALIDA_USUARIO"
                })
            }).then(response => response.json())
                .then(data => {
                    console.log("IMPRIME:" + data.MENSAJE)
                })
        }

        return this.setState({ error: '' });
    }

    handleUserChange(evt) {
        this.setState({
            username: evt.target.value,
        });
    };

    handlePassChange(evt) {
        this.setState({
            password: evt.target.value,
        });
    }

    render() {
        // NOTE: I use data-attributes for easier E2E testing
        // but you don't need to target those (any css-selector will work)

        return (
            <div>
                <div text-align="center" className="Title">
                    SISTEMA DE RESERVAS
            </div>

                <ProductSwitch onSubmit={this.handleSubmit}>
                    {
                        this.state.error &&
                        <h3 data-test="error" onClick={this.dismissError}>
                            <button onClick={this.dismissError}>✖</button>
                            {this.state.error}
                        </h3>
                    }
                    <div>

Usuario

                                <ui5-input type="text" data-test="username" value={this.state.username} onChange={this.handleUserChange} />

                    </div>
                    <div>
                        <label>Contraseña</label>
                        <ui5-input type="password" data-test="password" value={this.state.password} onChange={this.handlePassChange} />
                    </div>
                    <div>
                        <span>
                            <ui5-button design="Emphasized" block size="lg" type="submit" >
                                <span>
                                    Acceder
                                    </span>
                                <span>
                                    <ui5-icon class="samples-margin" name="initiative" ></ui5-icon>
                                    </span>
                    </ui5-button>
                        </span>

                    </div>

                </ProductSwitch>
                <LoginApp/>
            </div>

        );
    }
}
