
import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import './testLogin.css'
import './Login.js'


/* async function loginUser(credentials) {
    return fetch('http://localhost:80', {
        method: 'post',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            "CLASE": "ZCL_RESERVA",
            "ENTRADA": "{\"UNAME\":\"" + credentials.username + "\",\"PASSWORD\":\"" + credentials.password + "\"}",
            "METODO": "VALIDA_USUARIO"
        })
    }).then(data => data.json())
} */

async function loginUser(credentials) {
    return fetch('http://localhost:3000/login', {
        method: 'post',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            "name": credentials.username,
            "password": credentials.password 
        })
    }).then(data => data.json())
}

export default function LoginApp({ setToken }) {
    const [username, setUserName] = useState();
    const [password, setPassword] = useState();

    const handleSubmit = async e => {
        e.preventDefault();
        const token = await loginUser({
            username,
            password
        });
        console.log(username)
        console.log(password)
        console.log(token)
        //setToken(token);
    }
        return (
            <div className="login-wrapper">
                <h1>Please Log In</h1>
                <form onSubmit={handleSubmit}>
                    <label>
                        <p>Username</p>
                        <input type="text" onChange={e => setUserName(e.target.value)} />
                    </label>
                    <label>
                        <p>Password</p>
                        <input type="password" onChange={e => setPassword(e.target.value)} />
                    </label>
                    <div>
                        <button type="submit">Submit</button>
                    </div>
                </form>
            </div>
            
        );
    

}

LoginApp.propTypes = {
    //setToken: PropTypes.func.isRequired
}


