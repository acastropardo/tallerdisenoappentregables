import React, { Component } from 'react';
import { List, StandardListItem, Button, Text, Loader} from '@ui5/webcomponents-react';
import { SistemaAdministracion } from './SistemaAdministracion';

export class distribuidor extends Component {
  static displayName = distribuidor.name;
  

  constructor(props) {
    super(props);
    
    this.state = { currentCount: 0, ListMode:0 };
    this.selectedMode = this.state.ListMode;
    this.initialData = [];
    
    this.leerData();
  }
  
  async componentDidMount() {
    const res = await fetch('http://localhost:3000/get-all-distribuidor', {
      method: 'get',
      headers: { 'Content-Type': 'application/json' },
      // body: JSON.stringify({
      //     "CLASE": "ZCL_RESERVA",
      //     "ENTRADA": "{\"UNAME\":\"PEJANCUBE\",\"PASSWORD\":\"VENTISQUER\"}",
      //     "METODO": "VALIDA_USUARIO"
      // })
  });
    const data = await res.json();
    this.setState({ initialData: data });
  }

  async leerData(){
    fetch('http://localhost:3000/get-all-distribuidor', {
      method: 'get',
      headers: { 'Content-Type': 'application/json' },
      // body: JSON.stringify({
      //     "CLASE": "ZCL_RESERVA",
      //     "ENTRADA": "{\"UNAME\":\"PEJANCUBE\",\"PASSWORD\":\"VENTISQUER\"}",
      //     "METODO": "VALIDA_USUARIO"
      // })
  }).then(response => response.json())
      .then(data => {
          console.log(data)
          this.setState({ initialData: data });
          this.initialData = data
      })
  }

  render() {
    if(this.initialData.length == 0) {
      return <Loader></Loader>
    }
    else{
    return (
      <div>
                <SistemaAdministracion />

        <h3>Tabla distribuidor</h3>
        <List title={"distribuidor"} mode={this.selectedMode}>
        {this.initialData.map((distribuidor)=>(
          <StandardListItem description={"ID: " + distribuidor.iddistribuidor} 
          key={distribuidor.iddistribuidor}
            style={{ textAlign: 'left' }}>
            <Text>RUT:{distribuidor.rut} Nombre Empresa {distribuidor.nombre_empresa}</Text>
          </StandardListItem>
        ))}
        </List>
        <Button className="" icon="add" onClick={function noRefCheck(){}} slot="" style={{}} tooltip="">Agregar distribuidor</Button>
        <Button className="" icon="delete" onClick={function noRefCheck(){}} slot="" style={{}} tooltip="">Eliminar distribuidor</Button>

      </div>
    );
        }
  }
}