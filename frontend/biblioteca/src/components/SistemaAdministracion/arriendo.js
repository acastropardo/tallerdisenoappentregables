import React, { Component } from 'react';
import { List, StandardListItem, Button, Text, Loader, Table } from '@ui5/webcomponents-react';

export class Arriendo extends Component {
    static displayName = Arriendo.name;


    constructor(props) {
        super(props);

        this.state = { currentCount: 0, ListMode:0 };
        this.selectedMode = this.state.ListMode;
        this.initialData = [];

        this.leerData();
    }

    async componentDidMount() {
        const res = await fetch('http://localhost:3000/get-all-arriendo', {
            method: 'get',
            headers: { 'Content-Type': 'application/json' },
            // body: JSON.stringify({
            //     "CLASE": "ZCL_RESERVA",
            //     "ENTRADA": "{\"UNAME\":\"PEJANCUBE\",\"PASSWORD\":\"VENTISQUER\"}",
            //     "METODO": "VALIDA_USUARIO"
            // })
        });
        const data = await res.json();
        this.setState({ initialData: data });
    }

    async leerData() {
        fetch('http://localhost:3000/get-all-arriendo', {
            method: 'get',
            headers: { 'Content-Type': 'application/json' },
            // body: JSON.stringify({
            //     "CLASE": "ZCL_RESERVA",
            //     "ENTRADA": "{\"UNAME\":\"PEJANCUBE\",\"PASSWORD\":\"VENTISQUER\"}",
            //     "METODO": "VALIDA_USUARIO"
            // })
        }).then(response => response.json())
            .then(data => {
                console.log(data)
                this.setState({ initialData: data });
                this.initialData = data
            })
    }

    render() {
        if (this.initialData.length == 0) {
            return <Loader></Loader>
        }
        else {
            return (
                <div>

                    

                    <ui5-table class="demo-table" id="table" mode={this.selectedMode} align="left">
                        <ui5-table-column slot="columns"><span>Nº</span></ui5-table-column>
                        <ui5-table-column slot="columns"><span>Fecha Arriendo</span></ui5-table-column>
                        <ui5-table-column slot="columns"><span>Costo Arriendo</span></ui5-table-column>
                        <ui5-table-column slot="columns"><span>ISBN</span></ui5-table-column>
                        <ui5-table-column slot="columns"><span>Titulo</span></ui5-table-column>
                        <ui5-table-column slot="columns">Editorial</ui5-table-column>
                        <ui5-table-column slot="columns"><span>Categoria</span></ui5-table-column>
                        <ui5-table-column slot="columns">Idioma</ui5-table-column>
                        <ui5-table-column slot="columns">Precio Referencia</ui5-table-column>
                        <ui5-table-column slot="columns">RUT Cliente</ui5-table-column>
                        <ui5-table-column slot="columns">Nombre Cliente</ui5-table-column>
                        <ui5-table-column slot="columns">Fecha Entrega Estimada</ui5-table-column>
                        {this.initialData.map((arriendo) => (
                            <ui5-table-row>
                                <ui5-table-cell>{arriendo.idarriendo}</ui5-table-cell>
                                <ui5-table-cell>{arriendo.fecha_arriendo}</ui5-table-cell>
                                <ui5-table-cell>{arriendo.costo_arriendo}</ui5-table-cell>
                                <ui5-table-cell>{arriendo.libro_numero_serie}</ui5-table-cell>
                                <ui5-table-cell>{arriendo.titulo}</ui5-table-cell>
                                <ui5-table-cell>{arriendo.editorial}</ui5-table-cell>
                                <ui5-table-cell>{arriendo.categoria}</ui5-table-cell>
                                <ui5-table-cell>{arriendo.idioma}</ui5-table-cell>
                                <ui5-table-cell>{arriendo.precio_referencia}</ui5-table-cell>
                                <ui5-table-cell>{arriendo.rut}</ui5-table-cell>
                                <ui5-table-cell>{arriendo.nombres} {arriendo.apellido_paterno} {arriendo.apellido_materno}</ui5-table-cell>
                                <ui5-table-cell>{arriendo.fecha_devolucion_estimada}</ui5-table-cell>
                            </ui5-table-row>
                        ))}
                    </ui5-table>
  {/*                   <List title={"arriendo"} mode={this.selectedMode}>
                        {this.initialData.map((arriendo) => (
                            <StandardListItem description={"ID: " + arriendo.idarriendo}
                                key={arriendo.idarriendo}
                                style={{ textAlign: 'left' }}>
                                <Text>{arriendo.cliente_idcliente} {arriendo.fecha_arriendo}</Text>
                            </StandardListItem>
                        ))}
                    </List> */}

                </div>
            );
        }
    }
}