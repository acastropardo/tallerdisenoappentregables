import React, { Component } from 'react';
import { List, StandardListItem, Button, Text, Loader} from '@ui5/webcomponents-react';
import { SistemaAdministracion } from './SistemaAdministracion';


export class metodo_pago extends Component {
  static displayName = metodo_pago.name;
  

  constructor(props) {
    super(props);
    
    this.state = { currentCount: 0, ListMode:0 };
    this.selectedMode = this.state.ListMode;
    this.initialData = [];
    
    this.leerData();
  }
  
  async componentDidMount() {
    const res = await fetch('http://localhost:3000/get-all-metodo_pago', {
      method: 'get',
      headers: { 'Content-Type': 'application/json' },
      // body: JSON.stringify({
      //     "CLASE": "ZCL_RESERVA",
      //     "ENTRADA": "{\"UNAME\":\"PEJANCUBE\",\"PASSWORD\":\"VENTISQUER\"}",
      //     "METODO": "VALIDA_USUARIO"
      // })
  });
    const data = await res.json();
    this.setState({ initialData: data });
  }

  async leerData(){
    fetch('http://localhost:3000/get-all-metodo_pago', {
      method: 'get',
      headers: { 'Content-Type': 'application/json' },
      // body: JSON.stringify({
      //     "CLASE": "ZCL_RESERVA",
      //     "ENTRADA": "{\"UNAME\":\"PEJANCUBE\",\"PASSWORD\":\"VENTISQUER\"}",
      //     "METODO": "VALIDA_USUARIO"
      // })
  }).then(response => response.json())
      .then(data => {
          this.setState({ initialData: data });
          this.initialData = data
      })
  }

  render() {
    if(this.initialData.length == 0) {
      return <Loader></Loader>
    }
    else{
    return (
      <div>
                  <SistemaAdministracion />
        <h3>Tabla metodo_pago</h3>
        <List title={"metodo_pago"} mode={this.selectedMode}>
        {this.initialData.map((metodoPa)=>(
          <StandardListItem description={"ID: " + metodoPa.idmetodo_pago} 
          key={metodoPa.idmetodo_pago}
            style={{ textAlign: 'left' }}>
            <Text>{metodoPa.metodo_pago}</Text>
          </StandardListItem>
        ))}
        </List>
        <Button className="" icon="add" onClick={function noRefCheck(){}} slot="" style={{}} tooltip="">Agregar metodo_pago</Button>
        <Button className="" icon="delete" onClick={function noRefCheck(){}} slot="" style={{}} tooltip="">Eliminar metodo_pago</Button>
      </div>
    );
        }
  }
}