import React, { Component } from 'react';
import { List, StandardListItem, Button, Text, Loader} from '@ui5/webcomponents-react';
import { SistemaAdministracion } from './SistemaAdministracion';
export class Usuario extends Component {
  static displayName = Usuario.name;
  

  constructor(props) {
    super(props);
    
    this.state = { currentCount: 0, ListMode:0 };
    this.incrementCounter = this.incrementCounter.bind(this);
    this.selectedMode = this.state.ListMode;
    this.initialData = [];
    
    this.sampleData = [
        {
          "ProductId": "HT-1000",
          "Name": "Notebook Basic 15",
          "ProductPicUrl": "images/HT-1000.jpg",
        },
        {
          "ProductId": "HT-1001",
          "Name": "Notebook Basic 17",
          "ProductPicUrl": "images/HT-1001.jpg",
        },
        {
          "ProductId": "HT-1002",
          "Name": "Notebook Basic 18",
          "ProductPicUrl": "images/HT-1002.jpg",
        },
        {
          "ProductId": "HT-1003",
          "Category": "Laptops",
          "Name": "Notebook Basic 19",
          "ProductPicUrl": "images/HT-1003.jpg",
        }
  ];
  this.leerData();
  }
  
  incrementCounter() {
    this.setState({
      currentCount: this.state.currentCount + 1
    });


  }
  
  async componentDidMount() {
    const res = await fetch('http://localhost:3000/get-all-users', {
      method: 'get',
      headers: { 'Content-Type': 'application/json' },
      // body: JSON.stringify({
      //     "CLASE": "ZCL_RESERVA",
      //     "ENTRADA": "{\"UNAME\":\"PEJANCUBE\",\"PASSWORD\":\"VENTISQUER\"}",
      //     "METODO": "VALIDA_USUARIO"
      // })
  });
    const data = await res.json();
    this.setState({ initialData: data });
  }

  async leerData(){
    fetch('http://localhost:3000/get-all-users', {
      method: 'get',
      headers: { 'Content-Type': 'application/json' },
      // body: JSON.stringify({
      //     "CLASE": "ZCL_RESERVA",
      //     "ENTRADA": "{\"UNAME\":\"PEJANCUBE\",\"PASSWORD\":\"VENTISQUER\"}",
      //     "METODO": "VALIDA_USUARIO"
      // })
  }).then(response => response.json())
      .then(data => {
          console.log(data)
          this.setState({ initialData: data });
          this.initialData = data
      })
  }

  render() {
    if(this.initialData.length == 0) {
      return <Loader></Loader>
    }
    else{
    return (
      <div>
        <SistemaAdministracion></SistemaAdministracion>
        <h3>Usuarios</h3>
        <List title={"Usuarios"} mode={this.selectedMode}>
        {this.initialData.map((usuario)=>(
          <StandardListItem description={"ID: " + usuario.id+" - Email:"+usuario.email} 
          key={usuario.id}
            style={{ textAlign: 'left' }}>
            <Text>{usuario.name}</Text>
          </StandardListItem>
        ))}
        </List>
        <Button className="" icon="add" onClick={function noRefCheck(){}} slot="" style={{}} tooltip="">Agregar usuario</Button>
        <Button className="" icon="delete" onClick={function noRefCheck(){}} slot="" style={{}} tooltip="">Eliminar usuario</Button>

      </div>
    );
        }
  }
}