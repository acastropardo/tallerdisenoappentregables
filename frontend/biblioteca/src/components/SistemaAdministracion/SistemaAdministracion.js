import React, { Component, useState } from 'react';
//import { Route } from 'react-router';
import { ShellBar } from '@ui5/webcomponents-react';
import { StandardListItem } from '@ui5/webcomponents-react';
import { Avatar } from '@ui5/webcomponents-react';
//import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
//import { Dialog } from '@ui5/webcomponents-react';
//import { Button } from '@ui5/webcomponents-react';
//import { ShellBarItem } from '@ui5/webcomponents-react';
//import { Popover } from '@ui5/webcomponents-react';
import { useRef, useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation
} from "react-router-dom";

import { Usuario } from './usuario'


//import { MDXCreateElement } from '@ui5/webcomponents-react' ;

export function SistemaAdministracion(){

 /* const onClick = (e: React.MouseEvent<HTMLElement, MouseEvent>) => {
    popoverRef.current?.openBy(e.target);
  };*/
  const SistemaAdministracionRef = useRef();



  const handleItems = (e) =>{
    //alert(e.detail.item.attributes[2].nodeValue);

     // eslint-disable-next-line default-case
   switch(e.detail.item.attributes[2].nodeValue) {
      case '1':
        window.location.replace("/autor")
        break
      case '2':
        window.location.replace("/categoria")
        break
      case '3':
        window.location.replace("/distribuidor")
        break
      case '4':
        window.location.replace("/editorial")
        break
      case '5':
        window.location.replace("/estado_libro")
        break
      case '6':
        window.location.replace("/idioma")
        break
      case '7':
        window.location.replace("/metodo_pago")
        break
      case '8':
        window.location.replace("/usuario")
        break
    }
    
      
  }

    return(
<ShellBar ref={SistemaAdministracionRef}
  className=""
  //logo={<MDXCreateElement alt="SAP Logo" mdxType="img" originalType="img" src="https://sap.github.io/ui5-webcomponents/assets/images/sap-logo-svg.svg"/>}
  menuItems={<><StandardListItem key="1" data-key="1" >Mantenimiento tabla autor</StandardListItem>
  <StandardListItem key="2" data-key="2">Mantenimiento tabla categoria</StandardListItem>
  <StandardListItem key="3" data-key="3">Mantenimiento tabla distribuidor</StandardListItem>
  <StandardListItem key="4" data-key="4">Mantenimiento tabla editorial</StandardListItem>
  <StandardListItem key="5" data-key="5">Mantenimiento tabla estado_libro</StandardListItem>
  <StandardListItem key="6" data-key="6">Mantenimiento tabla idioma</StandardListItem>
  <StandardListItem key="7" data-key="7">Mantenimiento tabla metodo_pago</StandardListItem>
  <StandardListItem key="8" data-key="8">Mantenimiento tabla usuario</StandardListItem></>}
  notificationCount="10"
  onCoPilotClick={function noRefCheck(){}}
  //onLogoClick={() => window.location.replace("/formularioMarica")}
  onMenuItemClick={handleItems}
  onNotificationsClick={() => window.location.replace("/formularioMarica")}
  onProductSwitchClick={function noRefCheck(){}}
  onProfileClick={function noRefCheck(){}}
  primaryTitle="Mantenimiento de tablas Sistema Biblioteca"
  profile={<Avatar image="static/media/DemoImage.4b12bcf0.png" />}
  secondaryTitle="Fiori 3 Shell Bar"
  showCoPilot
  showNotifications
  showProductSwitch
  slot=""
  style={{}}
  tooltip=""
/>


    );
}



