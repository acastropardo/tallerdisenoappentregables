import React, { Component } from 'react';
import { List, StandardListItem, Button, Text, Loader} from '@ui5/webcomponents-react';
import { SistemaAdministracion } from './SistemaAdministracion';

export class Idioma extends Component {
  static displayName = Idioma.name;
  

  constructor(props) {
    super(props);

    this.state = { currentCount: 0, ListMode:0 };
    this.selectedMode = this.state.ListMode;
    this.initialData = [];
    
    this.leerData();
  }
  
  async componentDidMount() {
   
    const res = await fetch('http://localhost:3000/get-all-idioma', {
      method: 'get',
      headers: { 'Content-Type': 'application/json' },
      // body: JSON.stringify({
      //     "CLASE": "ZCL_RESERVA",
      //     "ENTRADA": "{\"UNAME\":\"PEJANCUBE\",\"PASSWORD\":\"VENTISQUER\"}",
      //     "METODO": "VALIDA_USUARIO"
      // })
  });
    const data = await res.json();
    this.setState({ initialData: data });
  }

  async leerData(){
    fetch('http://localhost:3000/get-all-idioma', {
      method: 'get',
      headers: { 'Content-Type': 'application/json' },
      // body: JSON.stringify({
      //     "CLASE": "ZCL_RESERVA",
      //     "ENTRADA": "{\"UNAME\":\"PEJANCUBE\",\"PASSWORD\":\"VENTISQUER\"}",
      //     "METODO": "VALIDA_USUARIO"
      // })
  }).then(response => response.json())
      .then(data => {
          console.log(data)
          this.setState({ initialData: data });
          this.initialData = data
      })
  }

  render() {
    if(this.initialData.length == 0) {
      return <Loader></Loader>
    }
    else{
    return (
      <div>
        <SistemaAdministracion />
        <h3>Tabla idioma</h3>
        <List title={"Idioma"} mode={this.selectedMode}>
        {this.initialData.map((idioma)=>(
          <StandardListItem description={"ID: " + idioma.idioma} 
          key={idioma.idioma}
            style={{ textAlign: 'left' }}>
            <Text>{idioma.idioma}</Text>
          </StandardListItem>
        ))}
        </List>
        <Button className="" icon="add" onClick={function noRefCheck(){}} slot="" style={{}} tooltip="">Agregar idioma</Button>
        <Button className="" icon="delete" onClick={function noRefCheck(){}} slot="" style={{}} tooltip="">Eliminar idioma</Button>

      </div>
    );
        }
  }
}