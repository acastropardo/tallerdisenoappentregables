import React, { Component } from 'react';
import { List, StandardListItem, ListMode, Text, Loader} from '@ui5/webcomponents-react';

export class Lista extends Component {
  static displayName = Lista.name;
  

  constructor(props) {
    super(props);
    
    this.state = { currentCount: 0, ListMode:0 };
    this.selectedMode = this.state.ListMode;
    this.initialData = [];
    
    this.leerData();
  }
  
  async componentDidMount() {
    const res = await fetch('http://localhost:3000/get-all-users', {
      method: 'get',
      headers: { 'Content-Type': 'application/json' },
      // body: JSON.stringify({
      //     "CLASE": "ZCL_RESERVA",
      //     "ENTRADA": "{\"UNAME\":\"PEJANCUBE\",\"PASSWORD\":\"VENTISQUER\"}",
      //     "METODO": "VALIDA_USUARIO"
      // })
  });
    const data = await res.json();
    this.setState({ initialData: data });
  }

  async leerData(){
    fetch('http://localhost:3000/get-all-users', {
      method: 'get',
      headers: { 'Content-Type': 'application/json' },
      // body: JSON.stringify({
      //     "CLASE": "ZCL_RESERVA",
      //     "ENTRADA": "{\"UNAME\":\"PEJANCUBE\",\"PASSWORD\":\"VENTISQUER\"}",
      //     "METODO": "VALIDA_USUARIO"
      // })
  }).then(response => response.json())
      .then(data => {
          console.log(data)
          this.setState({ initialData: data });
          this.initialData = data
      })
  }

  render() {
    if(this.initialData.length == 0) {
      return <Loader></Loader>
    }
    else{
    return (
      <div>
        <h3>Usuarios</h3>
        <List title={"Usuarios"} mode={this.selectedMode}>
        {this.initialData.map((usuario)=>(
          <StandardListItem description={"ID: " + usuario.iduser+" - Email:"+usuario.email} 
          key={usuario.id}
            style={{ textAlign: 'left' }}>
            <Text>{usuario.name}</Text>
          </StandardListItem>
        ))}
        </List>
      </div>
    );
        }
  }
}