import React, { Component, useState } from 'react';
import "@ui5/webcomponents/dist/Button";
import "@ui5/webcomponents/dist/Input.js";
import "@ui5/webcomponents/dist/Icon.js";
import '@ui5/webcomponents-icons/dist/json-imports/Icons.js';
import { ProductSwitch } from '@ui5/webcomponents-react';
import { FlexBox } from '@ui5/webcomponents-react';
import { Form } from '@ui5/webcomponents-react/dist/Form';
import { FormGroup } from '@ui5/webcomponents-react/dist/FormGroup';
import { FormItem } from '@ui5/webcomponents-react/dist/FormItem';
import { Input } from '@ui5/webcomponents-react/dist/Input';
import { InputType } from '@ui5/webcomponents-react/dist/InputType';
import { Label } from '@ui5/webcomponents-react/dist/Label';
import CheckBox from "@ui5/webcomponents/dist/CheckBox";
import Select from "@ui5/webcomponents/dist/Select";
import Option from "@ui5/webcomponents/dist/Option";
import TextArea from "@ui5/webcomponents/dist/TextArea";
import { Bar } from '@ui5/webcomponents-react';
import "@ui5/webcomponents/dist/List.js";
import "@ui5/webcomponents/dist/StandardListItem.js";
import "@ui5/webcomponents/dist/CustomListItem.js";
import { Arriendo } from "../SistemaAdministracion/arriendo"

import './StyleTest.css'

export class SistemaReservas extends Component {
    static displayName = SistemaReservas.name;
    
    constructor() {
        super();
    }
    render() {
        return(
            <div>
               <ui5-title level="H1">GESTION DE ARRIENDO DE LIBROS - BIBLIOTECA</ui5-title>
                <ui5-product-switch>
                    <ui5-product-switch-item heading="Ingresos" subtitle="Arrendar Libros" icon="sap-icon://add-activity"></ui5-product-switch-item>
                    <ui5-product-switch-item heading="Gestion" subtitle="Gestionar Arriendos" icon="sap-icon://activities"></ui5-product-switch-item>
                    <ui5-product-switch-item heading="Devoluciones" subtitle="Devolución de Libros Arrendados" icon="sap-icon://cause"></ui5-product-switch-item>
                </ui5-product-switch>

                <ui5-panel width="100%" accessible-role="Complementary" header-text="Detalle de Arriendos" class="full-width">
                <ui5-card avatar="group" heading="Listado de Libros Arrendados" status="3" class="medium" align="left">
                    <div class="card-content">
                    <Arriendo />

                    </div>
                </ui5-card>
                <ui5-card avatar="group" heading="Listado de Libros Disponibles" header-interactive subheading="Click, press Enter or Space" status="3 of 6" class="medium">
                    <div class="card-content">
                        <ui5-list separators="None" class="card-content-child" >

                        </ui5-list>
                    </div>
                </ui5-card>
                </ui5-panel>

                {/* <ui5-panel width="100%" accessible-role="Complementary" header-text="Both expandable and expanded" class="full-width">
                    <ui5-card> Hola</ui5-card>
                    <ui5-card> Hola</ui5-card>
                    <ui5-card> Hola</ui5-card>
                </ui5-panel> */}
             
            </div>

        );
    }
}

