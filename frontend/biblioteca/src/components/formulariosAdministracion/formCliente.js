import React, { Component, useState } from 'react';

import "@ui5/webcomponents/dist/Button";
import "@ui5/webcomponents/dist/Input.js";
import "@ui5/webcomponents/dist/Icon.js";
import '@ui5/webcomponents-icons/dist/json-imports/Icons.js';

import { FormItem } from '@ui5/webcomponents-react/dist/FormItem';


import "@ui5/webcomponents/dist/Table.js";
import "@ui5/webcomponents/dist/TableColumn.js";
import "@ui5/webcomponents/dist/TableRow.js";
import "@ui5/webcomponents/dist/TableCell.js";
import "@ui5/webcomponents-icons/dist/employee.js";
import "@ui5/webcomponents/dist/StepInput.js";
import "@ui5/webcomponents/dist/Toast";

import "@ui5/webcomponents/dist/features/InputElementsFormSupport.js";

export class formcliente extends Component {
    static displayName = formcliente.name;
    
    constructor() {
        super();
    }
    render() {
        return(
            <div class="FullBlock">

            <ui5-link href="/cliente" target="_self" design="Subtle" slot="startContent">Editar</ui5-link>
            <ui5-label id="basic-label" slot="startContent"> / </ui5-label>
            <ui5-link href="" target="_self" design="Subtle" slot="startContent" disabled> cliente</ui5-link>
        <ui5-title level="H1">Agregar/editar cliente</ui5-title>

        <ui5-panel fixed="true" width="100%" fixed="true" accessible-role="Complementary" header-text="Edición de entrada de tabla" >

            <form class="formTables" >


            <div class="row">
                    <FormItem>
                        <div class="left-column">
                            <ui5-label class="labelform" for="rut" required>RUT</ui5-label>
                             <div class="inputdescription">
                                <ui5-multi-input required={true} id="rut" /* show-value-help-icon value={this.state.centro} onClick={() => { document.getElementById("seleccion-centro-dialog").open() } }*/></ui5-multi-input>
                            </div> 
                        </div>
                    </FormItem>
                </div>

                <div class="row">
                    <FormItem>
                        <div class="left-column">
                            <ui5-label class="labelform" for="nombres" required>Nombres</ui5-label>
                             <div class="inputdescription">
                                <ui5-multi-input required={true} id="nombres" /* show-value-help-icon value={this.state.centro} onClick={() => { document.getElementById("seleccion-centro-dialog").open() } }*/></ui5-multi-input>
                            </div> 
                        </div>
                    </FormItem>
                </div>

                <div class="row">
                    <FormItem>
                        <div class="left-column">
                            <ui5-label class="labelform" for="apellido_materno" required>Apellido Materno</ui5-label>
                            <ui5-multi-input id="apellido_materno" /*show-value-help-icon  value={this.state.almacen} */ /* onClick={() => this.onClickAlmacen(this.state.centro)} */ required></ui5-multi-input>
                        </div>
                    </FormItem>
                </div>
                <div class="row">
                    <FormItem>
                        <div class="left-column">
                            <ui5-label class="labelform" for="apellido_paterno" required>Apellido Paterno</ui5-label>
                            <ui5-multi-input id="apellido_paterno" /*show-value-help-icon  value={this.state.almacen} */ /* onClick={() => this.onClickAlmacen(this.state.centro)} */ required></ui5-multi-input>
                        </div>
                    </FormItem>
                </div>
                <br></br>
    
                <div class="formButton">
                    <FormItem>

                        <ui5-button class="Grabar" id="Gravar" design="Emphasized" type="submit" icon="save" /* onClick={this.handleSubmitstock.bind(this)} */>Grabar</ui5-button>

                    </FormItem>
                </div>

            </form>
        </ui5-panel>


    </div>
        );
    }
}

