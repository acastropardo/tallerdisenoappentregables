import React, { Component, useState } from 'react';
import "@ui5/webcomponents/dist/Button";
import "@ui5/webcomponents/dist/Input.js";
import "@ui5/webcomponents/dist/Icon.js";
import '@ui5/webcomponents-icons/dist/json-imports/Icons.js';
import { ProductSwitch } from '@ui5/webcomponents-react';
import { FlexBox } from '@ui5/webcomponents-react';
import { Form } from '@ui5/webcomponents-react/dist/Form';
import { Button } from '@ui5/webcomponents-react';

import { FormGroup } from '@ui5/webcomponents-react/dist/FormGroup';
import { FormItem } from '@ui5/webcomponents-react/dist/FormItem';
import { Input } from '@ui5/webcomponents-react/dist/Input';
import { InputType } from '@ui5/webcomponents-react/dist/InputType';
import { Label } from '@ui5/webcomponents-react/dist/Label';
import CheckBox from "@ui5/webcomponents/dist/CheckBox";
import Select from "@ui5/webcomponents/dist/Select";
import Option from "@ui5/webcomponents/dist/Option";
import TextArea from "@ui5/webcomponents/dist/TextArea";

export class formDistribuidor extends Component {
    static displayName = formDistribuidor.name;
    
    constructor() {
        super();
    }
    render() {
        return(
            <div>
        <Form title="Crear/editar Distribuidor">
            <FormGroup title="Distribuidor">
                <FormItem label="RUT">
                    <Input />
                </FormItem>
                <FormItem label="Nombre empresa">
                    <Input />
                </FormItem>
                <FormItem label={<Label>Direccion</Label>}>
                    <Input />
                </FormItem>
                <FormItem label="Teléfono">
                    <Input />
                </FormItem>
                <FormItem label="Año inicio ventas">
                    <Input />
                </FormItem>
                </FormGroup>
                
        </Form>
        <Button className="" icon="save" onClick={function noRefCheck(){}} slot="" style={{}} tooltip="">Grabar Distribuidor</Button>
        </div>
        );
    }
}

