import React, { Component, useState } from 'react';
import "@ui5/webcomponents/dist/Button";
import "@ui5/webcomponents/dist/Input.js";
import "@ui5/webcomponents/dist/Icon.js";
import '@ui5/webcomponents-icons/dist/json-imports/Icons.js';
import { ProductSwitch } from '@ui5/webcomponents-react';
import { FlexBox } from '@ui5/webcomponents-react';
import { Form } from '@ui5/webcomponents-react/dist/Form';
import { Button } from '@ui5/webcomponents-react';

import { FormGroup } from '@ui5/webcomponents-react/dist/FormGroup';
import { FormItem } from '@ui5/webcomponents-react/dist/FormItem';
import { Input } from '@ui5/webcomponents-react/dist/Input';
import { InputType } from '@ui5/webcomponents-react/dist/InputType';
import { Label } from '@ui5/webcomponents-react/dist/Label';
import CheckBox from "@ui5/webcomponents/dist/CheckBox";
import Select from "@ui5/webcomponents/dist/Select";
import Option from "@ui5/webcomponents/dist/Option";
import TextArea from "@ui5/webcomponents/dist/TextArea";

export class formIdioma extends Component {
    static displayName = formIdioma.name;
    
    constructor() {
        super();
    }
    render() {
        return(
            <div class="FullBlock">

            <ui5-link href="/idioma" target="_self" design="Subtle" slot="startContent">Idioma</ui5-link>
            <ui5-label id="basic-label" slot="startContent"> / </ui5-label>
            <ui5-link href="" target="_self" design="Subtle" slot="startContent" disabled>Idioma</ui5-link>
        <ui5-title level="H1">Agregar/editar Idioma</ui5-title>

        <ui5-panel fixed="true" width="100%" fixed="true" accessible-role="Complementary" header-text="Edición de entrada de tabla" >

            <form class="formTables" >

                <div class="row">
                    <FormItem>
                        <div class="left-column">
                            <ui5-label class="labelform" for="idioma" required>Idioma</ui5-label>
                             <div class="inputdescription">
                                <ui5-multi-input required={true} id="idioma" /* show-value-help-icon value={this.state.centro} onClick={() => { document.getElementById("seleccion-centro-dialog").open() } }*/></ui5-multi-input>
                            </div> 
                        </div>
                    </FormItem>
                </div>
                <br></br>
                <div class="formButton">
                    <FormItem>
                        <ui5-button class="Grabar" id="Gravar" design="Emphasized" type="submit" icon="save" /* onClick={this.handleSubmitstock.bind(this)} */>Grabar</ui5-button>
                    </FormItem>
                </div>
            </form>
        </ui5-panel>
    </div>
        );
    }
}

