import React, { Component, useState } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { Counter } from './components/Counter';
import { Lista } from './components/Lista';
import { Login } from './components/LoginView/Login';
import { SistemaReservas } from './components/SistemaReservas/SistemaReservas'
import { SistemaAdministracion } from './components/SistemaAdministracion/SistemaAdministracion'
import { FormularioTest } from './components/formularioTest/formularioTest'
import { Usuario } from './components/SistemaAdministracion/usuario'
import { Autor } from './components/SistemaAdministracion/autor'
import { Cliente } from './components/SistemaAdministracion/cliente'
import { metodo_pago } from './components/SistemaAdministracion/metodo_pago'
import { Idioma } from './components/SistemaAdministracion/idioma'
import { distribuidor } from './components/SistemaAdministracion/distribuidor'
import { editorial } from './components/SistemaAdministracion/editorial'
import { estado_libro } from './components/SistemaAdministracion/estado_libro'
import { categoria } from './components/SistemaAdministracion/categoria'
import { formAutor } from './components/formulariosAdministracion/formAutor';
import { formcliente } from './components/formulariosAdministracion/formCliente';
import { formCategoria } from './components/formulariosAdministracion/formCategoria';
import { formIdioma } from './components/formulariosAdministracion/formIdioma';
import { formDistribuidor } from './components/formulariosAdministracion/formDistribuidor';
import { formEditorial } from './components/formulariosAdministracion/formEditorial';
import { formEstadoLibro } from './components/formulariosAdministracion/formEstadoLibro';
import { formMetodoPago } from './components/formulariosAdministracion/formMetodoPago';

//import { BrowserRouter, Switch } from 'react-router-dom';
//import LoginApp from './components/LoginView/LoginApp';
import useToken from './useToken';
import './custom.css'


function setToken(userToken) {
  sessionStorage.setItem('token', JSON.stringify(userToken));
}

function getToken() {
  const tokenString = sessionStorage.getItem('token');
  const userToken = JSON.parse(tokenString);
  return userToken?.token
}

export default function App() {

  const { token, setToken } = useToken();

//se comenta de manera temporal mientras se arregla el token
  /* if (!token) {
      console.log("ELTOKEN")
      console.log(token)
      return <LoginApp setToken={setToken} />
  } */
//fin de ese comentario
  return (
    <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/categoria' component={categoria} />
        <Route path='/counter' component={Counter} />
        <Route path='/lista' component={Lista} />
        <Route path='/fetch-data' component={FetchData} />
        <Route path='/login' component={Login} />
        <Route path='/arriendos' component={SistemaReservas} />
        <Route path='/formulariotest' component={FormularioTest} />
        <Route path='/administracion' component={SistemaAdministracion} />
        <Route path='/metodo_pago' component={metodo_pago} />
        <Route path='/usuario' component={Usuario} />
        <Route path='/autor' component={Autor} />
        <Route path='/cliente' component={Cliente} />
        <Route path='/idioma' component={Idioma} />
        <Route path='/distribuidor' component={distribuidor} />
        <Route path='/editorial' component={editorial} />
        <Route path='/estado_libro' component={estado_libro} />
        <Route path='/form_autor' component={formAutor} />
        <Route path='/form_cliente' component={formcliente} />
        <Route path='/form_categoria' component={formCategoria} />
        <Route path='/form_idioma' component={formIdioma} />
        <Route path='/form_distribuidor' component={formDistribuidor} />
        <Route path='/form_editorial' component={formEditorial} />
        <Route path='/form_estado_libro' component={formEstadoLibro} />
        <Route path='/form_metodo_pago' component={formMetodoPago} />
    </Layout>
  );

  }
/*  
export default class App extends Component {
    static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/counter' component={Counter} />
        <Route path='/lista' component={Lista} />
        <Route path='/fetch-data' component={FetchData} />
        <Route path='/login' component={Login} />
        <Route path='/reservas' component={SistemaReservas} />
        <Route path='/administracion' component={SistemaAdministracion} />
      </Layout>
    );
  }
}


function App2() {
    const [token, setToken] = useState();

    if (!token) {
        return <Login setToken={setToken} />
    }

    return (
        <div >

        </div>
    );
}
*/
