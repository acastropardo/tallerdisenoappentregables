import { useState } from 'react';

export default function useToken() {
    const getToken = () => {
        const tokenString = localStorage.getItem('token');
        const userToken = JSON.parse(tokenString);
        console.log("gettoken retorna")
        console.log(userToken?.COD)
        //return userToken?.token
        return userToken?.COD
    };

    const [token, setToken] = useState(getToken());

    console.log("token")
    console.log(token)
 

    const saveToken = userToken => {
        localStorage.setItem('token', JSON.stringify(userToken));
        console.log("USERTOKEN")
        console.log(userToken.token)
        setToken(userToken.token);
    };

    console.log("SAVETOKEN")
    console.log(saveToken)

    return {
        setToken: saveToken,
        token
    }
}