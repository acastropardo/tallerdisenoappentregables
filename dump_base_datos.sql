-- --------------------------------------------------------
-- Host:                         192.168.68.114
-- Server version:               10.6.3-MariaDB-1:10.6.3+maria~focal - mariadb.org binary distribution
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping data for table mydb.arriendo: ~2 rows (approximately)
DELETE FROM `arriendo`;
/*!40000 ALTER TABLE `arriendo` DISABLE KEYS */;
INSERT INTO `arriendo` (`idarriendo`, `costo_total`, `fecha_arriendo`, `fecha_devolucion_estimada`, `fecha_entrega_real`, `dias_retraso`, `multa`, `costo_arriendo`, `cliente_idcliente`, `trabajador_idtrabajador`) VALUES
	(1, 5000, '2021-08-05', '2021-08-21', '1900-01-01', 0, 0, 5000, 1, 1),
	(2, 7500, '2021-08-01', '2021-09-01', '1900-01-01', 0, 0, 7500, 2, 1);
/*!40000 ALTER TABLE `arriendo` ENABLE KEYS */;

-- Dumping data for table mydb.autor: ~1 rows (approximately)
DELETE FROM `autor`;
/*!40000 ALTER TABLE `autor` DISABLE KEYS */;
INSERT INTO `autor` (`idautor`, `nombres`, `apellido_paterno`, `apellido_materno`) VALUES
	(1, 'Gabriel', 'Garcia', 'Marquez'),
	(2, 'Francis', 'Ford', 'Coppola');
/*!40000 ALTER TABLE `autor` ENABLE KEYS */;

-- Dumping data for table mydb.boleta: ~0 rows (approximately)
DELETE FROM `boleta`;
/*!40000 ALTER TABLE `boleta` DISABLE KEYS */;
/*!40000 ALTER TABLE `boleta` ENABLE KEYS */;

-- Dumping data for table mydb.categoria: ~11 rows (approximately)
DELETE FROM `categoria`;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` (`idcategoria`, `categoria`) VALUES
	(1, 'Geografía'),
	(2, 'Matemáticas'),
	(3, 'Álgebra'),
	(4, 'Ciencias'),
	(5, 'Física'),
	(6, 'Astronomía'),
	(7, 'Biología'),
	(8, 'Antropología'),
	(9, 'Ciencias Políticas'),
	(10, 'Medicina'),
	(11, 'Informática');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;

-- Dumping data for table mydb.cliente: ~1 rows (approximately)
DELETE FROM `cliente`;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` (`idcliente`, `rut`, `nombres`, `apellido_paterno`, `apellido_materno`, `fecha_nacimiento`, `telefono_idtelefono`, `direccion_iddireccion`, `correo_electronico_idcorreo_electronico`) VALUES
	(1, '25042776-K', 'Daniel', 'Castro', 'Paillacan', '2015-07-13', 1, 1, 1),
	(2, '23018996-K', 'Abraham', 'Castro', 'Pardo', '2020-01-01', 1, 1, 1);
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;

-- Dumping data for table mydb.compra: ~0 rows (approximately)
DELETE FROM `compra`;
/*!40000 ALTER TABLE `compra` DISABLE KEYS */;
/*!40000 ALTER TABLE `compra` ENABLE KEYS */;

-- Dumping data for table mydb.correo_electronico: ~0 rows (approximately)
DELETE FROM `correo_electronico`;
/*!40000 ALTER TABLE `correo_electronico` DISABLE KEYS */;
INSERT INTO `correo_electronico` (`idcorreo_electronico`, `correo_electronico`) VALUES
	(1, 'N/A');
/*!40000 ALTER TABLE `correo_electronico` ENABLE KEYS */;

-- Dumping data for table mydb.detalle_arriendo: ~3 rows (approximately)
DELETE FROM `detalle_arriendo`;
/*!40000 ALTER TABLE `detalle_arriendo` DISABLE KEYS */;
INSERT INTO `detalle_arriendo` (`iddetalle_arriendo`, `arriendo_idarriendo`, `libro_idlibro`, `libro_numero_serie`) VALUES
	(1, 1, 1, '1232132131'),
	(2, 1, 2, '3425325423'),
	(5, 2, 3, '1232123131');
/*!40000 ALTER TABLE `detalle_arriendo` ENABLE KEYS */;

-- Dumping data for table mydb.detalle_compra: ~0 rows (approximately)
DELETE FROM `detalle_compra`;
/*!40000 ALTER TABLE `detalle_compra` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalle_compra` ENABLE KEYS */;

-- Dumping data for table mydb.detalle_venta: ~0 rows (approximately)
DELETE FROM `detalle_venta`;
/*!40000 ALTER TABLE `detalle_venta` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalle_venta` ENABLE KEYS */;

-- Dumping data for table mydb.direccion: ~0 rows (approximately)
DELETE FROM `direccion`;
/*!40000 ALTER TABLE `direccion` DISABLE KEYS */;
INSERT INTO `direccion` (`iddireccion`, `direccion`) VALUES
	(1, 'N/A');
/*!40000 ALTER TABLE `direccion` ENABLE KEYS */;

-- Dumping data for table mydb.distribuidor: ~0 rows (approximately)
DELETE FROM `distribuidor`;
/*!40000 ALTER TABLE `distribuidor` DISABLE KEYS */;
/*!40000 ALTER TABLE `distribuidor` ENABLE KEYS */;

-- Dumping data for table mydb.editorial: ~4 rows (approximately)
DELETE FROM `editorial`;
/*!40000 ALTER TABLE `editorial` DISABLE KEYS */;
INSERT INTO `editorial` (`ideditorial`, `editorial`) VALUES
	(1, 'Prentice Hall'),
	(2, 'Mc Graw Hill'),
	(3, 'Planeta'),
	(4, 'Wrox');
/*!40000 ALTER TABLE `editorial` ENABLE KEYS */;

-- Dumping data for table mydb.estado_libro: ~4 rows (approximately)
DELETE FROM `estado_libro`;
/*!40000 ALTER TABLE `estado_libro` DISABLE KEYS */;
INSERT INTO `estado_libro` (`idestado_libro`, `estado_libro`) VALUES
	(1, 'Vendido'),
	(2, 'Perdido'),
	(3, 'Prestado'),
	(4, 'Disponible'),
	(5, 'Retorno');
/*!40000 ALTER TABLE `estado_libro` ENABLE KEYS */;

-- Dumping data for table mydb.factura: ~0 rows (approximately)
DELETE FROM `factura`;
/*!40000 ALTER TABLE `factura` DISABLE KEYS */;
/*!40000 ALTER TABLE `factura` ENABLE KEYS */;

-- Dumping data for table mydb.idioma: ~6 rows (approximately)
DELETE FROM `idioma`;
/*!40000 ALTER TABLE `idioma` DISABLE KEYS */;
INSERT INTO `idioma` (`ididioma`, `idioma`) VALUES
	(1, 'Portugues'),
	(2, 'Ingles'),
	(3, 'Español'),
	(4, 'Aleman'),
	(5, 'Frances'),
	(6, 'Italiano'),
	(7, 'Ruso');
/*!40000 ALTER TABLE `idioma` ENABLE KEYS */;

-- Dumping data for table mydb.libro: ~3 rows (approximately)
DELETE FROM `libro`;
/*!40000 ALTER TABLE `libro` DISABLE KEYS */;
INSERT INTO `libro` (`idlibro`, `numero_serie`, `titulo`, `no_paginas`, `precio_referencia`, `annio`, `cantidad_stock_venta`, `cantidad_stock_prestamo`, `categoria_idcategoria`, `autor_idautor`, `estado_libro_idestado_libro`, `idioma_ididioma`, `editorial_ideditorial`) VALUES
	(1, '1232132131', 'Programación en Python', 1200, 50000, '2020-01-01', 10, 2, 11, 1, 4, 1, 1),
	(2, '3425325423', 'Programación en Java', 900, 45000, '2012-01-01', 5, 10, 11, 1, 4, 2, 2),
	(3, '1232123131', 'Programación en C++', 800, 40000, '2010-01-01', 5, 8, 11, 2, 4, 3, 2);
/*!40000 ALTER TABLE `libro` ENABLE KEYS */;

-- Dumping data for table mydb.metodo_pago: ~4 rows (approximately)
DELETE FROM `metodo_pago`;
/*!40000 ALTER TABLE `metodo_pago` DISABLE KEYS */;
INSERT INTO `metodo_pago` (`idmetodo_pago`, `metodo_pago`) VALUES
	(1, 'Debito'),
	(2, 'Credito'),
	(3, 'Efectivo'),
	(4, 'Cheque');
/*!40000 ALTER TABLE `metodo_pago` ENABLE KEYS */;

-- Dumping data for table mydb.telefono: ~0 rows (approximately)
DELETE FROM `telefono`;
/*!40000 ALTER TABLE `telefono` DISABLE KEYS */;
INSERT INTO `telefono` (`idtelefono`, `numero`) VALUES
	(1, 'N/A');
/*!40000 ALTER TABLE `telefono` ENABLE KEYS */;

-- Dumping data for table mydb.trabajador: ~0 rows (approximately)
DELETE FROM `trabajador`;
/*!40000 ALTER TABLE `trabajador` DISABLE KEYS */;
INSERT INTO `trabajador` (`idtrabajador`, `rut`, `apellido_paterno`, `apellido_materno`, `fecha_contrato`, `telefono_idtelefono`, `direccion_iddireccion`, `correo_electronico_idcorreo_electronico1`) VALUES
	(1, '23018996-K', 'Castro', 'Pardo', '2020-01-01', 1, 1, 1);
/*!40000 ALTER TABLE `trabajador` ENABLE KEYS */;

-- Dumping data for table mydb.users: ~2 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES
	(1, 'Abraham', 'abraham.castro@icloud.com', '1307'),
	(2, 'Daniel', 'daniel.castro@icloud.com', '1307'),
	(3, 'Beatriz', 'beatriz.pmorales@icloud.com', '1307');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping data for table mydb.venta: ~0 rows (approximately)
DELETE FROM `venta`;
/*!40000 ALTER TABLE `venta` DISABLE KEYS */;
/*!40000 ALTER TABLE `venta` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
